package com.formula.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.formula.functions.Util;
import com.formula.hibernate.Family;
import com.formula.hibernate.model.IMgmt_Family;

@ManagedBean(name="family_bean")
@ViewScoped
public class FamilyBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Family selectedFamily = new Family();
    private List<Family> lstFamily;
    private String action;
	@ManagedProperty(value="#{mgmt_family}")
	private IMgmt_Family mgmt_family;

	public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.clear();
        this.action = action;
    }

    public List<Family> getLstFamily() {
        return lstFamily;
    }

    public void setLstFamily(List<Family> lstFamily) {
        this.lstFamily = lstFamily;
    }

    public Family getSelectedFamily() {
        return selectedFamily;
    }

    public void setSelectedFamily(Family selectedFamily) {
        this.selectedFamily = selectedFamily;
    }
    
    public IMgmt_Family getMgmt_family() {
		return mgmt_family;
	}

	public void setMgmt_family(IMgmt_Family mgmt_family) {
		this.mgmt_family = mgmt_family;
	}

	private boolean isPostBack(){
        boolean answer;
        answer = FacesContext.getCurrentInstance().isPostback();
        return answer;
    }
    
    public void perform() throws Exception{
        switch(action){
            case "Create":
                this.create();
                this.clear();
                break;
            case "Update":
                this.update();
                this.clear();
                break;
        }
    }
    
    public void clear(){
    	if(selectedFamily==null) {
    		selectedFamily = new Family();
    	}
        this.selectedFamily.setFamily_id(0L);;
        this.selectedFamily.setFamily_name("");
    }
    
    private void create() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.family.create.message");
		
        try {
        	mgmt_family.create_Family(selectedFamily);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
    
    public void reading(String value) throws Exception{
        try {
            if(value.equals("F")){
                if(isPostBack() == false){
                    lstFamily = mgmt_family.reading_All();
                }
            } else {
                    lstFamily = mgmt_family.reading_All();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void update() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.family.edit.message");
		
        try {
        	mgmt_family.update_Family(selectedFamily);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }

    public void delete() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.family.delete.message");
		
        try {
        	mgmt_family.delete_Family(selectedFamily);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
}