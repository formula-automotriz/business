package com.formula.managedbean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import com.formula.functions.Util;
import com.formula.hibernate.Product;
import com.formula.hibernate.model.IMgmt_Product;

// Adding the combo functionality
import com.formula.hibernate.Brand;
import com.formula.hibernate.Family;
import com.formula.hibernate.Line;
import com.formula.hibernate.Measure;
import com.formula.hibernate.Metric;
import com.formula.hibernate.Price;
import com.formula.hibernate.User;
import com.formula.hibernate.model.IMgmt_Brand;
import com.formula.hibernate.model.IMgmt_Document;
import com.formula.hibernate.model.IMgmt_Family;
import com.formula.hibernate.model.IMgmt_Line;
import com.formula.hibernate.model.IMgmt_Measure;
import com.formula.hibernate.model.IMgmt_Metric;
import com.formula.hibernate.model.IMgmt_Price;

@ManagedBean(name="product_bean")
@ViewScoped
public class ProductBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Product selectedProduct = new Product();
    private List<Product> lstProduct;
    private String action;
	@ManagedProperty(value="#{mgmt_price}")
	private IMgmt_Price mgmt_price;
	@ManagedProperty(value="#{mgmt_product}")
	private IMgmt_Product mgmt_product;

	public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.clear();
        this.action = action;
    }

    public List<Product> getLstProduct() {
        return lstProduct;
    }

    public void setLstProduct(List<Product> lstProduct) {
        this.lstProduct = lstProduct;
    }

    public Product getSelectedProduct() {
        return selectedProduct;
    }

    public void setSelectedProduct(Product selectedProduct) {
        this.selectedProduct = selectedProduct;
    }

    public IMgmt_Price getMgmt_price() {
		return mgmt_price;
	}

	public void setMgmt_price(IMgmt_Price mgmt_price) {
		this.mgmt_price = mgmt_price;
	}

    public IMgmt_Product getMgmt_product() {
		return mgmt_product;
	}

	public void setMgmt_product(IMgmt_Product mgmt_product) {
		this.mgmt_product = mgmt_product;
	}

	private boolean isPostBack(){
        boolean answer;
        answer = FacesContext.getCurrentInstance().isPostback();
        return answer;
    }
    
    public void perform() throws Exception{
        switch(action){
            case "Create":
                this.create();
                this.clear();
                break;
            case "Update":
                this.update();
                this.clear();
                break;
        }
    }
    
    public void clear(){
    	if(selectedProduct==null) {
    		selectedProduct = new Product();
    	}
        this.selectedProduct.setProduct_id(0L);
        this.selectedProduct.setProduct_code("");
        this.selectedProduct.setProduct_name("");
        this.selectedProduct.setContent(0.0D);
        this.selectedProduct.setBar_code("");
        
     // Adding the combo functionality
        this.brand = null;
        this.family = null;
        this.line = null;
        this.metric = null;
        this.measure = null;
        this.price = null;
        this.cost = null;
        this.profit = null;
        this.selling = null;
        this.price_date = null;
    }
    
    private void create() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.product.create.message");
		
        try {
        	selectedProduct.setPrice(priceChanged());
        	mgmt_product.create_Product(selectedProduct);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }

    private void update() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.product.edit.message");
		
        try {
        	selectedProduct.setPrice(priceChanged());
        	mgmt_product.update_Product(selectedProduct);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
    
    public void reading(String value) throws Exception{
        try {
            if(value.equals("F")){
                if(isPostBack() == false){
                    lstProduct = mgmt_product.reading_All();
                }
            } else {
                    lstProduct = mgmt_product.reading_All();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void delete() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.product.delete.message");
		
        try {
        	mgmt_product.delete_Product(selectedProduct);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }


	
 // Adding the combo functionality
	private Brand brand = new Brand();
	private Family family = new Family();
	private Line line = new Line();
	private Measure measure = new Measure();
	private Metric metric = new Metric();
	private Price price;
	private Double cost;
	private Double profit;
	private Double selling;
	private Date price_date;
	private String document;

    private List<Brand> lstBrand;
    private List<Family> lstFamily;
    private List<Line> lstLine;
    private List<Measure> lstMeasure;
    private List<Metric> lstMetric;
    private List<Price> lstPrice;
	
	@ManagedProperty(value="#{mgmt_brand}")
	private IMgmt_Brand mgmt_brand;
	
	@ManagedProperty(value="#{mgmt_family}")
	private IMgmt_Family mgmt_family;

	@ManagedProperty(value="#{mgmt_line}")
	private IMgmt_Line mgmt_line;
	
	@ManagedProperty(value="#{mgmt_measure}")
	private IMgmt_Measure mgmt_measure;
	
	@ManagedProperty(value="#{mgmt_metric}")
	private IMgmt_Metric mgmt_metric;
	
	@ManagedProperty(value="#{mgmt_document}")
	private IMgmt_Document mgmt_document;

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public Family getFamily() {
		return family;
	}

	public void setFamily(Family family) {
		this.family = family;
	}

	public Line getLine() {
		return line;
	}

	public void setLine(Line line) {
		this.line = line;
	}

	public Measure getMeasure() {
		return measure;
	}

	public void setMeasure(Measure measure) {
		this.measure = measure;
	}

	public Metric getMetric() {
		return metric;
	}

	public void setMetric(Metric metric) {
		this.metric = metric;
		metricChanged(this.metric);
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Double getProfit() {
		return profit;
	}

	public void setProfit(Double profit) {
		this.profit = profit;
	}

	public Double getSelling() {
		return selling;
	}

	public void setSelling(Double selling) {
		this.selling = selling;
	}

	public Date getPrice_date() {
		return price_date==null?new Date():price_date;
	}

	public void setPrice_date(Date price_date) {
		this.price_date = price_date;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public List<Brand> getLstBrand() {
		return lstBrand;
	}

	public void setLstBrand(List<Brand> lstBrand) {
		this.lstBrand = lstBrand;
	}

	public List<Family> getLstFamily() {
		return lstFamily;
	}

	public void setLstFamily(List<Family> lstFamily) {
		this.lstFamily = lstFamily;
	}

	public List<Line> getLstLine() {
		return lstLine;
	}

	public void setLstLine(List<Line> lstLine) {
		this.lstLine = lstLine;
	}

	public List<Measure> getLstMeasure() {
		return lstMeasure;
	}

	public void setLstMeasure(List<Measure> lstMeasure) {
		this.lstMeasure = lstMeasure;
	}

	public List<Metric> getLstMetric() {
		return lstMetric;
	}

	public void setLstMetric(List<Metric> lstMetric) {
		this.lstMetric = lstMetric;
	}

	public List<Price> getLstPrice() {
		return lstPrice;
	}

	public void setLstPrice(List<Price> lstPrice) {
		this.lstPrice = lstPrice;
	}

	public IMgmt_Brand getMgmt_brand() {
		return mgmt_brand;
	}

	public void setMgmt_brand(IMgmt_Brand mgmt_brand) {
		this.mgmt_brand = mgmt_brand;
	}

	public IMgmt_Family getMgmt_family() {
		return mgmt_family;
	}

	public void setMgmt_family(IMgmt_Family mgmt_family) {
		this.mgmt_family = mgmt_family;
	}

	public IMgmt_Line getMgmt_line() {
		return mgmt_line;
	}

	public void setMgmt_line(IMgmt_Line mgmt_line) {
		this.mgmt_line = mgmt_line;
	}
	
    public IMgmt_Measure getMgmt_measure() {
		return mgmt_measure;
	}

	public void setMgmt_measure(IMgmt_Measure mgmt_measure) {
		this.mgmt_measure = mgmt_measure;
	}

	public IMgmt_Metric getMgmt_metric() {
		return mgmt_metric;
	}

	public void setMgmt_metric(IMgmt_Metric mgmt_metric) {
		this.mgmt_metric = mgmt_metric;
	}

	public IMgmt_Document getMgmt_document() {
		return mgmt_document;
	}

	public void setMgmt_document(IMgmt_Document mgmt_document) {
		this.mgmt_document = mgmt_document;
	}

	public void readingCombo(String value) throws Exception{
        try {
            if(value.equals("F")){
                if(isPostBack() == false){
                    lstBrand = mgmt_brand.reading_All();
                    lstFamily = mgmt_family.reading_All();
                    lstLine = mgmt_line.reading_All();
                    lstMeasure = mgmt_measure.reading_All();
                    lstMetric = mgmt_metric.reading_All();
                }
            } else {
                    lstBrand = mgmt_brand.reading_All();
                    lstFamily = mgmt_family.reading_All();
                    lstLine = mgmt_line.reading_All();
                    lstMeasure = mgmt_measure.reading_All();
                    lstMetric = mgmt_metric.reading_All();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void brandChanged(ValueChangeEvent e) { 
    	  
    	brand = (Brand)e.getNewValue();
    	if(brand != null) {
            lstLine = mgmt_line.reading_ByProperty("brand.brand_id", brand.getBrand_id());
    	}
        family = null;
        line = null;
    }

    public void familyChanged(ValueChangeEvent e) { 
    	  
    	family = (Family)e.getNewValue();
    	if(family != null) {
            lstLine = mgmt_line.reading_ByProperty("family.family_id", family.getFamily_id());
    	}
        brand = null;
        line = null;
    }

    public void lineChanged(ValueChangeEvent e) { 
    	  
    	line = (Line)e.getNewValue();
    	if(line != null) {
        	brand=line.getBrand();
        	family=line.getFamily();
        	selectedProduct.setLine(line);
    	}
    	else {
    		brand = null;
    		family = null;
    		lstLine = mgmt_line.reading_All();
    	}
    }

    public void metricChanged(ValueChangeEvent e) { 
    	metricChanged((Metric)e.getNewValue());
        measure = null;
    }

    public void measureChanged(ValueChangeEvent e) { 
    	  
    	measure = (Measure)e.getNewValue();
    	if(measure != null) {
        	metric=measure.getMetric();
        	selectedProduct.setMeasure(measure);
    	}
    	else {
    		metric = null;
    		lstMeasure = mgmt_measure.reading_All();
    	}
    }
    
    public void metricChanged(Metric metric) {
    	this.metric = metric;
    	if(this.metric != null) {
            lstMeasure = mgmt_measure.reading_ByProperty("metric.metric_id", this.metric.getMetric_id());
    	}
    	else {
    		lstMeasure = mgmt_measure.reading_All();
    	}
    }
    
    public Price priceChanged() {
    	price = null;
		switch(action){
        case "Create":
        	createPrice();
            break;
        case "Update":
        	if(selectedProduct.getPrice()!=null) {
            	lstPrice = mgmt_price.reading_ByProductDate(selectedProduct, price_date);
            	if(lstPrice.size()>0) {
            		price = lstPrice.get(0);
            		price.setDocument(mgmt_document.reading_ByCode(document));
            		price.setCost(cost);
            		price.setProfit(profit);
            		price.setSelling(selling);
            		price.setPrice_date(price_date);
            		price.setProduct(selectedProduct);
            		price.setUser((User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user"));
            		mgmt_price.update_Price(price);
            	}
            	else {
                	createPrice();
            	}
        	}
        	else {
        		if(cost!=null || profit!=null || selling!=null) {
                	createPrice();
            	}
        	}
            break;
        }
    	return price;
    }

    public void createPrice() {
        price = new Price();
        Long price_id = mgmt_price.getMax()!=null?mgmt_price.getMax():0;
		price.setPrice_id((Long)price_id+1);
		price.setDocument(mgmt_document.reading_ByCode(document));
		price.setCost(cost);
		price.setProfit(profit);
		price.setSelling(selling);
		price.setPrice_date(price_date);
		price.setProduct(selectedProduct);
		price.setUser((User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user"));
		mgmt_price.create_Price(price);
    }
}