package com.formula.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import com.formula.functions.Util;
import com.formula.hibernate.Line;
import com.formula.hibernate.model.IMgmt_Line;

@ManagedBean(name="line_bean")
@ViewScoped
public class LineBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Line selectedLine = new Line();
    private List<Line> lstLine;
    private String action;
	@ManagedProperty(value="#{mgmt_line}")
	private IMgmt_Line mgmt_line;

	public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.clear();
        this.action = action;
    }

    public List<Line> getLstLine() {
        return lstLine;
    }

    public void setLstLine(List<Line> lstLine) {
        this.lstLine = lstLine;
    }

    public Line getSelectedLine() {
        return selectedLine;
    }

    public void setSelectedLine(Line selectedLine) {
        this.selectedLine = selectedLine;
    }
    
    public IMgmt_Line getMgmt_line() {
		return mgmt_line;
	}

	public void setMgmt_line(IMgmt_Line mgmt_line) {
		this.mgmt_line = mgmt_line;
	}

	private boolean isPostBack(){
        boolean answer;
        answer = FacesContext.getCurrentInstance().isPostback();
        return answer;
    }
    
    public void perform() throws Exception{
        switch(action){
            case "Create":
                this.create();
                this.clear();
                break;
            case "Update":
                this.update();
                this.clear();
                break;
        }
    }
    
    public void clear(){
    	if(selectedLine==null) {
    		selectedLine = new Line();
    	}
        this.selectedLine.setLine_id(0L);;
        this.selectedLine.setLine_name("");
    }
    
    private void create() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.line.create.message");
		
        try {
        	mgmt_line.create_Line(selectedLine);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
    
    public void reading(String value) throws Exception{
        try {
            if(value.equals("F")){
                if(isPostBack() == false){
                    lstLine = mgmt_line.reading_All();
                }
            } else {
                    lstLine = mgmt_line.reading_All();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void update() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.line.edit.message");
		
        try {
        	mgmt_line.update_Line(selectedLine);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }

    public void delete() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.line.delete.message");
		
        try {
        	mgmt_line.delete_Line(selectedLine);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
    
    public void updateFromBrand(AjaxBehaviorEvent e) {
        try {
        	Line line = (Line) e.getComponent().getAttributes().get("brandSelected");
             lstLine = mgmt_line.reading_ByProperty("brand.brand_id", line.getBrand().getBrand_id());
        } catch (Exception ex) {
            throw ex;
        }    	
    }
    public void updateFromFamily(AjaxBehaviorEvent e) {
        try {
        	Line line = (Line) e.getComponent().getAttributes().get("familySelected");
             lstLine = mgmt_line.reading_ByProperty("family.family_id", line.getFamily().getFamily_id());
        } catch (Exception ex) {
            throw ex;
        }    	
    }
}