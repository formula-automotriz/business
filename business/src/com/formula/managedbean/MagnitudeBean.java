package com.formula.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.formula.functions.Util;
import com.formula.hibernate.Magnitude;
import com.formula.hibernate.model.IMgmt_Magnitude;

@ManagedBean(name="magnitude_bean")
@ViewScoped
public class MagnitudeBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Magnitude selectedMagnitude = new Magnitude();
    private List<Magnitude> lstMagnitude;
    private String action;
	@ManagedProperty(value="#{mgmt_magnitude}")
	private IMgmt_Magnitude mgmt_magnitude;

	public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.clear();
        this.action = action;
    }

    public List<Magnitude> getLstMagnitude() {
        return lstMagnitude;
    }

    public void setLstMagnitude(List<Magnitude> lstMagnitude) {
        this.lstMagnitude = lstMagnitude;
    }

    public Magnitude getSelectedMagnitude() {
        return selectedMagnitude;
    }

    public void setSelectedMagnitude(Magnitude selectedMagnitude) {
        this.selectedMagnitude = selectedMagnitude;
    }
    
    public IMgmt_Magnitude getMgmt_magnitude() {
		return mgmt_magnitude;
	}

	public void setMgmt_magnitude(IMgmt_Magnitude mgmt_magnitude) {
		this.mgmt_magnitude = mgmt_magnitude;
	}

	private boolean isPostBack(){
        boolean answer;
        answer = FacesContext.getCurrentInstance().isPostback();
        return answer;
    }
    
    public void perform() throws Exception{
        switch(action){
            case "Create":
                this.create();
                this.clear();
                break;
            case "Update":
                this.update();
                this.clear();
                break;
        }
    }
    
    public void clear(){
    	if(selectedMagnitude==null) {
    		selectedMagnitude = new Magnitude();
    	}
        this.selectedMagnitude.setMagnitude_id(0L);
        this.selectedMagnitude.setMagnitude_name("");
    }
    
    private void create() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.magnitude.create.message");
		
        try {
        	mgmt_magnitude.create_Magnitude(selectedMagnitude);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
    
    public void reading(String value) throws Exception{
        try {
            if(value.equals("F")){
                if(isPostBack() == false){
                    lstMagnitude = mgmt_magnitude.reading_All();
                }
            } else {
                    lstMagnitude = mgmt_magnitude.reading_All();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void update() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.magnitude.edit.message");
		
        try {
        	mgmt_magnitude.update_Magnitude(selectedMagnitude);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }

    public void delete() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.magnitude.delete.message");
		
        try {
        	mgmt_magnitude.delete_Magnitude(selectedMagnitude);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
}