package com.formula.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.formula.functions.Util;
import com.formula.hibernate.Metric;
import com.formula.hibernate.model.IMgmt_Metric;

@ManagedBean(name="metric_bean")
@ViewScoped
public class MetricBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Metric selectedMetric = new Metric();
    private List<Metric> lstMetric;
    private String action;
	@ManagedProperty(value="#{mgmt_metric}")
	private IMgmt_Metric mgmt_metric;

	public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.clear();
        this.action = action;
    }

    public List<Metric> getLstMetric() {
        return lstMetric;
    }

    public void setLstMetric(List<Metric> lstMetric) {
        this.lstMetric = lstMetric;
    }

    public Metric getSelectedMetric() {
        return selectedMetric;
    }

    public void setSelectedMetric(Metric selectedMetric) {
        this.selectedMetric = selectedMetric;
    }
    
    public IMgmt_Metric getMgmt_metric() {
		return mgmt_metric;
	}

	public void setMgmt_metric(IMgmt_Metric mgmt_metric) {
		this.mgmt_metric = mgmt_metric;
	}

	private boolean isPostBack(){
        boolean answer;
        answer = FacesContext.getCurrentInstance().isPostback();
        return answer;
    }
    
    public void perform() throws Exception{
        switch(action){
            case "Create":
                this.create();
                this.clear();
                break;
            case "Update":
                this.update();
                this.clear();
                break;
        }
    }
    
    public void clear(){
    	if(selectedMetric==null) {
    		selectedMetric = new Metric();
    	}
        this.selectedMetric.setMetric_id(0L);
        this.selectedMetric.setMetric_name("");
    }
    
    private void create() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.metric.create.message");
		
        try {
        	mgmt_metric.create_Metric(selectedMetric);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
    
    public void reading(String value) throws Exception{
        try {
            if(value.equals("F")){
                if(isPostBack() == false){
                    lstMetric = mgmt_metric.reading_All();
                }
            } else {
                    lstMetric = mgmt_metric.reading_All();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void update() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.metric.edit.message");
		
        try {
        	mgmt_metric.update_Metric(selectedMetric);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }

    public void delete() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.metric.delete.message");
		
        try {
        	mgmt_metric.delete_Metric(selectedMetric);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
}