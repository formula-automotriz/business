package com.formula.managedbean;

import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;

@ManagedBean(name="utility_bean")
@ViewScoped
public class UtilityBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String changeNumeric;
	private ArrayList<String> cols = new ArrayList<String>();

	public String getChangeNumeric() {
		return changeNumeric;
	}

	public void setChangeNumeric(String changeNumeric) {
		this.cols.add(changeNumeric);
	}

    public void postProcessXLS(Object document) {
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
    	HSSFRow row = sheet.getRow(0);
        HSSFCellStyle cellStyle = wb.createCellStyle();

        cellStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.TURQUOISE.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        ArrayList<Long> columns = new ArrayList<Long>();
        
        for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
            HSSFCell cell = row.getCell(i);
            for(int x = 0; x < cols.size(); x++) {
	            if(row.getCell(i).getStringCellValue().equals(cols.get(x))) {
	            	columns.add(Long.valueOf(i));
	            }
            }
            cell.setCellStyle(cellStyle);
        }
        
        cellStyle = wb.createCellStyle();
        cellStyle.setDataFormat(wb.createDataFormat().getFormat("#,##0.000"));
        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
        	row = sheet.getRow(i);
        	for(int j = 0; j < columns.size(); j++) {
	            HSSFCell cell = row.getCell(columns.get(j).intValue());
	            if(cell.getStringCellValue().isEmpty()) {
	            	cell.setCellValue("0.0");
	            }
	            cell.setCellValue(Double.parseDouble(cell.getStringCellValue()));
	            cell.setCellType(CellType.NUMERIC);
	            cell.setCellStyle(cellStyle);
        	}
        }
        columns = new ArrayList<Long>();
        cols = new ArrayList<String>();
        changeNumeric = new String();
    }
}