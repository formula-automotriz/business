package com.formula.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.formula.functions.Util;
import com.formula.hibernate.Measure;
import com.formula.hibernate.model.IMgmt_Measure;

@ManagedBean(name="measure_bean")
@ViewScoped
public class MeasureBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Measure selectedMeasure = new Measure();
    private List<Measure> lstMeasure;
    private String action;
	@ManagedProperty(value="#{mgmt_measure}")
	private IMgmt_Measure mgmt_measure;

	public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.clear();
        this.action = action;
    }

    public List<Measure> getLstMeasure() {
        return lstMeasure;
    }

    public void setLstMeasure(List<Measure> lstMeasure) {
        this.lstMeasure = lstMeasure;
    }

    public Measure getSelectedMeasure() {
        return selectedMeasure;
    }

    public void setSelectedMeasure(Measure selectedMeasure) {
        this.selectedMeasure = selectedMeasure;
    }
    
    public IMgmt_Measure getMgmt_measure() {
		return mgmt_measure;
	}

	public void setMgmt_measure(IMgmt_Measure mgmt_measure) {
		this.mgmt_measure = mgmt_measure;
	}

	private boolean isPostBack(){
        boolean answer;
        answer = FacesContext.getCurrentInstance().isPostback();
        return answer;
    }
    
    public void perform() throws Exception{
        switch(action){
            case "Create":
                this.create();
                this.clear();
                break;
            case "Update":
                this.update();
                this.clear();
                break;
        }
    }
    
    public void clear(){
    	if(selectedMeasure==null) {
    		selectedMeasure = new Measure();
    	}
        this.selectedMeasure.setMeasure_id(0L);
        this.selectedMeasure.setMeasure_code("");
        this.selectedMeasure.setMeasure_ext("");
    }
    
    private void create() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.measure.create.message");
		
        try {
        	mgmt_measure.create_Measure(selectedMeasure);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
    
    public void reading(String value) throws Exception{
        try {
            if(value.equals("F")){
                if(isPostBack() == false){
                    lstMeasure = mgmt_measure.reading_All();
                }
            } else {
                    lstMeasure = mgmt_measure.reading_All();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void update() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.measure.edit.message");
		
        try {
        	mgmt_measure.update_Measure(selectedMeasure);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }

    public void delete() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.measure.delete.message");
		
        try {
        	mgmt_measure.delete_Measure(selectedMeasure);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
}