package com.formula.managedbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.shaded.json.JSONArray;

import com.formula.hibernate.Product;

@ManagedBean(name="backing_bean")
@ViewScoped
public class BackingBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Product> data;
	private Double randomValue;

	public List<Product> getData() {
	    return data;
	}

	public void setData(List<Product> data) {
	    this.data = data;
	}

	public void updateData(){
	    Map<String, String> paramValues = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
	    String json = paramValues.get("data");
	    JSONArray table = new JSONArray(json);
	    data = new ArrayList<>();
	    for (int i = 0; i < table.length(); i++){
	        JSONArray row = table.getJSONArray(i);
	        Product t = new Product();

	        for (int j = 0; j < row.length(); j++ ){
	            String o = row.getString(j);
	            if (j == 0){
	            	t.setProduct_code(o);
	            } else if((j == 1)) {
	                t.setProduct_name(o);
	            } else if((j == 2)) {
	                t.setContent(Double.parseDouble(o));
	            } else {
	            	t.setBar_code(o);
	            }
	        }

	        data.add(t);

	    }
	}
	
	public Double getRandomValue() {
		return randomValue;
	}

	public void processRandomNumber() {
	    Random random = new Random();
	    this.randomValue = random.nextDouble();
	}
}