package com.formula.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.formula.functions.Util;
import com.formula.hibernate.Category;
import com.formula.hibernate.model.IMgmt_Category;

@ManagedBean(name="category_bean")
@ViewScoped
public class CategoryBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Category selectedCategory = new Category();
    private List<Category> lstCategory;
    private String action;
	@ManagedProperty(value="#{mgmt_category}")
	private IMgmt_Category mgmt_category;

	public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.clear();
        this.action = action;
    }

    public List<Category> getLstCategory() {
        return lstCategory;
    }

    public void setLstCategory(List<Category> lstCategory) {
        this.lstCategory = lstCategory;
    }

    public Category getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(Category selectedCategory) {
        this.selectedCategory = selectedCategory;
    }
    
    public IMgmt_Category getMgmt_category() {
		return mgmt_category;
	}

	public void setMgmt_category(IMgmt_Category mgmt_category) {
		this.mgmt_category = mgmt_category;
	}

	private boolean isPostBack(){
        boolean answer;
        answer = FacesContext.getCurrentInstance().isPostback();
        return answer;
    }
    
    public void perform() throws Exception{
        switch(action){
            case "Create":
                this.create();
                this.clear();
                break;
            case "Update":
                this.update();
                this.clear();
                break;
        }
    }
    
    public void clear(){
    	if(selectedCategory==null) {
    		selectedCategory = new Category();
    	}
        this.selectedCategory.setCategory_id(0L);;
        this.selectedCategory.setCategory_name("");
    }
    
    private void create() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.category.create.message");
		
        try {
        	mgmt_category.create_Category(selectedCategory);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
    
    public void reading(String value) throws Exception{
        try {
            if(value.equals("F")){
                if(isPostBack() == false){
                    lstCategory = mgmt_category.reading_All();
                }
            } else {
                    lstCategory = mgmt_category.reading_All();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void update() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.category.edit.message");
		
        try {
        	mgmt_category.update_Category(selectedCategory);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }

    public void delete() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.category.delete.message");
		
        try {
        	mgmt_category.delete_Category(selectedCategory);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
}