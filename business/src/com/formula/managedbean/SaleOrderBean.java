package com.formula.managedbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.primefaces.event.CellEditEvent;

import com.formula.functions.Util;
import com.formula.hibernate.Sale_Order;
import com.formula.hibernate.model.IMgmt_Sale_Order;

import com.formula.hibernate.Product;
import com.formula.hibernate.Sale_Order_Detail;
import com.formula.hibernate.Client;
import com.formula.hibernate.Measure;
import com.formula.hibernate.Metric;
import com.formula.hibernate.model.IMgmt_Sale_Order_Detail;
import com.formula.hibernate.model.IMgmt_Product;
import com.formula.hibernate.model.IMgmt_Client;
import com.formula.hibernate.model.IMgmt_Metric;
import com.formula.hibernate.model.IMgmt_Measure;

@ManagedBean(name="sale_order_bean")
@ViewScoped
public class SaleOrderBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Sale_Order selectedSaleOrder = new Sale_Order();
    private List<Sale_Order> lstSaleOrder;
    private String action;
	@ManagedProperty(value="#{mgmt_sale_order}")
	private IMgmt_Sale_Order mgmt_sale_order;

	public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
        this.clear();
    }

    public List<Sale_Order> getLstSaleOrder() {
        return lstSaleOrder;
    }

    public void setLstSaleOrder(List<Sale_Order> lstSaleOrder) {
        this.lstSaleOrder = lstSaleOrder;
    }

    public Sale_Order getSelectedSaleOrder() {
        return selectedSaleOrder;
    }

    public void setSelectedSaleOrder(Sale_Order selectedSaleOrder) {
        this.selectedSaleOrder = selectedSaleOrder;
    }
    
    public IMgmt_Sale_Order getMgmt_sale_order() {
		return mgmt_sale_order;
	}

	public void setMgmt_sale_order(IMgmt_Sale_Order mgmt_sale_order) {
		this.mgmt_sale_order = mgmt_sale_order;
	}

	private boolean isPostBack(){
        boolean answer;
        answer = FacesContext.getCurrentInstance().isPostback();
        return answer;
    }
    
    public void perform() throws Exception{
        switch(action){
            case "Create":
                this.create();
                this.clear();
                break;
            case "Update":
                this.update();
                this.clear();
                break;
        }
    }
    
    private void clear(){
    	this.selectedSaleOrder = new Sale_Order();
    	this.preSaleOrder.setClient(new Client());
    	this.product = new Product();
    	this.lstSaleOrderDetail = new ArrayList<Sale_Order_Detail>();
    	this.preSaleOrder.setOrder_date(new Date());
    	this.measure = null;
    	this.total = 0.0;
    	if(action.equals("Create"))
    		this.selectedSaleOrder.setOrder_id((mgmt_sale_order.getMax()!=null?mgmt_sale_order.getMax():0)+1);
    }
    
    public void add() {
		Sale_Order_Detail od = new Sale_Order_Detail();
		od.setQuantity(quantity);
		od.setProduct(product);
		od.setMeasure(measure);
		if(product.getMeasure().equals(measure)) {
			od.setSelling(product.getPrice().getSelling());
		}
    	else {
    		od.setSelling(product.getPrice().getSelling()*measure.getContent());
		}
		total += od.getSelling()*od.getQuantity();
        selectedSaleOrder.setOrder_total(total);
        selectedSaleOrder.setOrder_subtotal(selectedSaleOrder.getOrder_total()/(1+0.18));
        selectedSaleOrder.setOrder_tax(selectedSaleOrder.getOrder_total()-selectedSaleOrder.getOrder_subtotal());
        selectedSaleOrder.setOrder_payment(total);
		this.lstSaleOrderDetail.add(od);
		this.product = null;
		this.quantity = null;
		this.measure = null;
		this.lstMeasure = null;
	}

    public void remove(Sale_Order_Detail od) {
		total -= od.getSelling()*od.getQuantity();
        selectedSaleOrder.setOrder_total(total);
        selectedSaleOrder.setOrder_subtotal(selectedSaleOrder.getOrder_total()/(1+0.18));
        selectedSaleOrder.setOrder_tax(selectedSaleOrder.getOrder_total()-selectedSaleOrder.getOrder_subtotal());
        selectedSaleOrder.setOrder_payment(total);
		this.lstSaleOrderDetail.remove(od);
    }

    private void create() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.sale_order.create.message");
		
		double value = 0.0;
		Long order_id = mgmt_sale_order.getMax()!=null?mgmt_sale_order.getMax():0;
		Long detail_id = mgmt_sale_order_detail.getMax()!=null?mgmt_sale_order_detail.getMax():0;
		
        try {
        	for(Sale_Order_Detail det : lstSaleOrderDetail) {
        		value += det.getSelling()*det.getQuantity();
        	}
        	selectedSaleOrder.setClient(preSaleOrder.getClient());
        	selectedSaleOrder.setOrder_total(value);
        	selectedSaleOrder.setOrder_subtotal(selectedSaleOrder.getOrder_total()/(1+0.18));
        	selectedSaleOrder.setOrder_tax(selectedSaleOrder.getOrder_total()-selectedSaleOrder.getOrder_subtotal());
        	selectedSaleOrder.setOrder_id((Long)order_id+1);
        	selectedSaleOrder.setOrder_date(preSaleOrder.getOrder_date());
        	selectedSaleOrder.setOrder_payment(preSaleOrder.getOrder_payment());
        	mgmt_sale_order.create_Sale_Order(selectedSaleOrder);
        	for(Sale_Order_Detail det : lstSaleOrderDetail) {
        		detail_id++;
        		det.setDetail_id(detail_id);
        		det.setSale_order(selectedSaleOrder);
        		mgmt_sale_order_detail.create_Sale_Order_Detail(det);
        	}
        	selectedSaleOrder.setDetails(new HashSet<>(lstSaleOrderDetail));
        	mgmt_sale_order.update_Sale_Order(selectedSaleOrder);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
    
    public void reading(String value) throws Exception{
        try {
            if(value.equals("F")){
                if(isPostBack() == false){
                    lstSaleOrder = mgmt_sale_order.reading_All();
                }
            } else {
                    lstSaleOrder = mgmt_sale_order.reading_All();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void update() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.sale_order.edit.message");

		double value = 0.0;

        try {
        	if(!detailsAreTheSame() || !selectedSaleOrder.getClient().equals(preSaleOrder.getClient()) || !selectedSaleOrder.getOrder_date().equals(preSaleOrder.getOrder_date()) || !selectedSaleOrder.getOrder_payment().equals(preSaleOrder.getOrder_payment())) {
        		for(Sale_Order_Detail det : lstSaleOrderDetail) {
	        		value += det.getSelling()*det.getQuantity();
	        	}
	        	selectedSaleOrder.setClient(preSaleOrder.getClient());
	        	selectedSaleOrder.setOrder_total(value);
	        	selectedSaleOrder.setOrder_subtotal(selectedSaleOrder.getOrder_total()/(1+0.18));
	        	selectedSaleOrder.setOrder_tax(selectedSaleOrder.getOrder_total()-selectedSaleOrder.getOrder_subtotal());
	        	selectedSaleOrder.setOrder_date(preSaleOrder.getOrder_date());
	        	//selectedSaleOrder.setOrder_payment(preSaleOrder.getOrder_payment());
	
	        	deleteDetails(selectedSaleOrder.getOrder_id());
	        	Long detail_id = mgmt_sale_order_detail.getMax()!=null?mgmt_sale_order_detail.getMax():0;
	        	for(Sale_Order_Detail det : lstSaleOrderDetail) {
	        		detail_id++;
	        		det.setSale_order(selectedSaleOrder);
	        		det.setDetail_id(detail_id);
	        		mgmt_sale_order_detail.create_Sale_Order_Detail(det);
	        	}
	        	selectedSaleOrder.setDetails(new HashSet<Sale_Order_Detail>(lstSaleOrderDetail));
	        	mgmt_sale_order.update_Sale_Order(selectedSaleOrder);
	            this.reading("T");
	            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        	}
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }

    public void delete() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.sale_order.delete.message");

        try {
        	mgmt_sale_order.delete_Sale_Order(selectedSaleOrder);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }

    public void deleteDetails(Long order_id) {
    	List<Sale_Order_Detail> temp = mgmt_sale_order_detail.reading_List(order_id);
    	for(Sale_Order_Detail det : temp) {
    		mgmt_sale_order_detail.delete_Sale_Order_Detail(det);
    	}
    }

    public void onEditCell(CellEditEvent<?> event) {
        Double oldValue = (double)event.getOldValue();
        Double newValue = (double)event.getNewValue();

        if(newValue != null && !newValue.equals(oldValue)) {
        	
            Sale_Order_Detail tmp = this.lstSaleOrderDetail.get(event.getRowIndex());
            tmp.setSelling(newValue);
            this.lstSaleOrderDetail.set(event.getRowIndex(), tmp);
        	this.total = 0.0;
            for(Sale_Order_Detail det : this.lstSaleOrderDetail){
                total += det.getSelling() * det.getQuantity();
            }
            selectedSaleOrder.setOrder_total(total);
            selectedSaleOrder.setOrder_subtotal(selectedSaleOrder.getOrder_total()/(1+0.18));
            selectedSaleOrder.setOrder_tax(selectedSaleOrder.getOrder_total()-selectedSaleOrder.getOrder_subtotal());
            //selectedSaleOrder.setOrder_payment(selectedSaleOrder.getOrder_total());
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Precio cambiado", "Antes: " + oldValue + ", Después:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void articuloSeleccionado(ValueChangeEvent event) {
    	this.setProduct((Product)event.getNewValue());
    	this.resetMeasure();
    }
    public void resetMeasure() {
    	if(product!=null) {
    		if(product.getMeasure()!=null) {
    			if(product.getMeasure().getMetric()!=null) {
    				if(product.getMeasure().getMetric().getMagnitude()!=null) {
    			    	if(product.getMeasure().getMetric().getMagnitude() != null) {
    			    		if(product.getType()!=null) {
    			    			switch(product.getType()) {
	    			    			case 0:{ //entire
	    			    				this.lstMeasure = new ArrayList<Measure>();
	    			    				this.lstMeasure.add(product.getMeasure());
	    			    				break;
	    			    			}
	    			    			case 1:{ //partition
	        			    			lstMetric = mgmt_metric.reading_ByProperty("magnitude.magnitude_id", product.getMeasure().getMetric().getMagnitude().getMagnitude_id());
	        	    			    	List<Measure> lstTemp = new ArrayList<Measure>();
	        	    			    	for(Metric metric : this.lstMetric) {
	        	    			    		Measure measure = new Measure();
	        	    			    		measure.setMeasure_id(metric.getMetric_id());
	        	    			    		measure.setMeasure_code(metric.getMetric_name());
	        	    			    		lstTemp.add(measure);
	        	    			    	}
	        	    			    	this.lstMeasure = lstTemp;
	    			    				break;
	    			    			}
    			    			}
    			    		}
    			    		else {
    			    			lstMetric = new ArrayList<Metric>();
    			    		}
    			    	}
    				}
    			}
    		}
    	}
    	this.measure = null;
    }

    public void load_Id(Sale_Order sale_order) throws Exception{
        try {
        	this.lstSaleOrderDetail = mgmt_sale_order_detail.reading_List(sale_order.getOrder_id());
        	this.preSaleOrder.setClient(mgmt_client.reading_ById(sale_order.getClient().getClient_id()));
        	sale_order.setClient(preSaleOrder.getClient());
        	this.total = 0.0;
            this.selectedSaleOrder = sale_order;
            this.preSaleOrder.setOrder_date(sale_order.getOrder_date());
            this.preSaleOrder.setOrder_payment(sale_order.getOrder_payment());
            preSaleOrderDetail = new ArrayList<Sale_Order_Detail>();
        	List<Sale_Order_Detail> lstSaleOrderDetail = new ArrayList<Sale_Order_Detail>();
            for(Sale_Order_Detail det : this.lstSaleOrderDetail){
            	Sale_Order_Detail pre = new Sale_Order_Detail();
            	pre.setDetail_id(det.getDetail_id());
            	pre.setDiscount(det.getDiscount());
            	pre.setSale_order(det.getSale_order());
            	pre.setSelling(det.getSelling());
                this.product = mgmt_product.reading_ById(det.getProduct().getProduct_id());
                this.measure = mgmt_measure.reading_ById(det.getMeasure().getMeasure_id());
                if(det.getQuantity()==null) {
                	det.setQuantity(0.0D);
                	pre.setQuantity(0.0D);
                }
                else {
                	pre.setQuantity(det.getQuantity());
                }
                det.setProduct(this.product);
                pre.setProduct(this.product);
                det.setMeasure(this.measure);
                pre.setMeasure(this.measure);
                //total += det.getSelling() * det.getQuantity();
                lstSaleOrderDetail.add(det);
                preSaleOrderDetail.add(pre);
            }
            total = sale_order.getOrder_total();
            this.lstSaleOrderDetail = lstSaleOrderDetail;
            this.action = "Update";
            this.product = new Product();
            this.measure = null;
            this.product.setMeasure(measure);
            this.lstMeasure = new ArrayList<Measure>();
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void measureChanged(ValueChangeEvent e) {
    	Measure temp = (Measure)e.getNewValue();
    	if(temp != null) {
    		if(this.measure == null) {
	    		if(product.getType()!=null) {
	    			switch(product.getType()) {
		    			case 0:{ //entire
			    			this.lstMeasure = new ArrayList<Measure>();
		    				this.lstMeasure.add(product.getMeasure());
		    				break;
		    			}
		    			case 1:{ //partition
		        			lstMeasure = mgmt_measure.reading_ByProperty("metric.metric_id", temp.getMeasure_id());
		    				break;
		    			}
	    			}
	    		}
	    		else {
	    			lstMeasure = new ArrayList<Measure>();
	    			this.measure = null;
	    		}
    		}
    		else {
    			this.measure = temp;
    		}
    	}
    	else {
    		this.resetMeasure();
    	}
    }
    
    public boolean detailsAreTheSame() {
    	boolean result = false;

    	if (lstSaleOrderDetail.size()!=preSaleOrderDetail.size()) {
    		return false;
    	}
    	for(int index=0; index<lstSaleOrderDetail.size(); index++) {
    		if(preSaleOrderDetail.get(index).getDetail_id()==lstSaleOrderDetail.get(index).getDetail_id()&&
	    		preSaleOrderDetail.get(index).getDiscount()==lstSaleOrderDetail.get(index).getDiscount()&&
	    		preSaleOrderDetail.get(index).getMeasure().equals(lstSaleOrderDetail.get(index).getMeasure())&&
	    		preSaleOrderDetail.get(index).getProduct().equals(lstSaleOrderDetail.get(index).getProduct())&&
	    		preSaleOrderDetail.get(index).getQuantity()==lstSaleOrderDetail.get(index).getQuantity()&&
	    		preSaleOrderDetail.get(index).getSale_order().equals(lstSaleOrderDetail.get(index).getSale_order())&&
	    		preSaleOrderDetail.get(index).getSelling()==lstSaleOrderDetail.get(index).getSelling()) {
    			result = true;
    		}
    		else {
    			return false;
    		}
    	}
    	
    	return result;
    }

    public void handleSave() {
    	selectedSaleOrder.setDiscounted_amount(selectedSaleOrder.getOrder_total() - selectedSaleOrder.getOrder_payment());
    	selectedSaleOrder.setOrder_discount(selectedSaleOrder.getDiscounted_amount()/selectedSaleOrder.getOrder_total());
	}

    private Product product = new Product();
    private Measure measure = new Measure();
    private Sale_Order_Detail sale_order_detail = new Sale_Order_Detail();
    private Double quantity;
    private Sale_Order preSaleOrder = new Sale_Order();
    private List<Sale_Order_Detail> preSaleOrderDetail;
    private List<Sale_Order_Detail> lstSaleOrderDetail = new ArrayList<Sale_Order_Detail>();
    private List<Measure> lstMeasure = new ArrayList<Measure>();
    private List<Metric> lstMetric;
    private Double total;
	@ManagedProperty(value="#{mgmt_sale_order_detail}")
	private IMgmt_Sale_Order_Detail mgmt_sale_order_detail;
	@ManagedProperty(value="#{mgmt_product}")
	private IMgmt_Product mgmt_product;
	@ManagedProperty(value="#{mgmt_measure}")
	private IMgmt_Measure mgmt_measure;
	@ManagedProperty(value="#{mgmt_metric}")
	private IMgmt_Metric mgmt_metric;
	@ManagedProperty(value="#{mgmt_client}")
	private IMgmt_Client mgmt_client;

	public Sale_Order getPreSaleOrder() {
		return preSaleOrder;
	}

	public void setPreSaleOrder(Sale_Order preSaleOrder) {
		this.preSaleOrder = preSaleOrder;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Measure getMeasure() {
		return measure;
	}

	public void setMeasure(Measure measure) {
		this.measure = measure;
	}

	public Sale_Order_Detail getSale_order_detail() {
		return sale_order_detail;
	}

	public void setSale_order_detail(Sale_Order_Detail sale_order_detail) {
		this.sale_order_detail = sale_order_detail;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public List<Sale_Order_Detail> getLstSaleOrderDetail() {
		return lstSaleOrderDetail;
	}

	public void setLstSaleOrderDetail(List<Sale_Order_Detail> lstSaleOrderDetail) {
		this.lstSaleOrderDetail = lstSaleOrderDetail;
	}

	public List<Measure> getLstMeasure() {
		return lstMeasure;
	}

	public void setLstMeasure(List<Measure> lstMeasure) {
		this.lstMeasure = lstMeasure;
	}

	public List<Metric> getLstMetric() {
		return lstMetric;
	}

	public void setLstMetric(List<Metric> lstMetric) {
		this.lstMetric = lstMetric;
	}

	public IMgmt_Sale_Order_Detail getMgmt_sale_order_detail() {
		return mgmt_sale_order_detail;
	}

	public void setMgmt_sale_order_detail(IMgmt_Sale_Order_Detail mgmt_sale_order_detail) {
		this.mgmt_sale_order_detail = mgmt_sale_order_detail;
	}

	public IMgmt_Product getMgmt_product() {
		return mgmt_product;
	}

	public void setMgmt_product(IMgmt_Product mgmt_product) {
		this.mgmt_product = mgmt_product;
	}

	public IMgmt_Measure getMgmt_measure() {
		return mgmt_measure;
	}

	public void setMgmt_measure(IMgmt_Measure mgmt_measure) {
		this.mgmt_measure = mgmt_measure;
	}

	public IMgmt_Metric getMgmt_metric() {
		return mgmt_metric;
	}

	public void setMgmt_metric(IMgmt_Metric mgmt_metric) {
		this.mgmt_metric = mgmt_metric;
	}

	public IMgmt_Client getMgmt_client() {
		return mgmt_client;
	}

	public void setMgmt_client(IMgmt_Client mgmt_client) {
		this.mgmt_client = mgmt_client;
	}
}