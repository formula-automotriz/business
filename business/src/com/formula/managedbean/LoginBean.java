package com.formula.managedbean;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.formula.functions.Util;
import com.formula.hibernate.User;
import com.formula.hibernate.model.IMgmt_User;

@ManagedBean(name = "login_bean")
@ViewScoped
public class LoginBean implements Serializable {

	private static final long serialVersionUID = 1L;
	// PROPIEDADES PARA RECIBIR LOS VALORES DE LOS COMPONENTES
	private String user_name;
	private String user_password;
	private User user;
	// FACHADA DE LA CAPA MODELO
	@ManagedProperty("#{mgmt_user}")
	private IMgmt_User mgmt_user;

	@PostConstruct
	public void init() {
		user = new User();
	}

	public String login() {
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String warn = Util.getKey("warn.credentials.title");
		String warn_message = Util.getKey("warn.credentials.description");
		String redireccion = null;
		try {
			User us;
			us = mgmt_user.login(user);
			if(us != null) {
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", us);
				redireccion = "index?faces-redirect=true";
			} else {
	            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,warn,warn_message));
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
		}
		return redireccion;
	}

	public void validateSesion() {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			User us = (User) context.getExternalContext().getSessionMap().get("user");
			if(us == null) {
				context.getExternalContext().redirect("login.xhtml");
				context.responseComplete();
			}
			else {
				this.user = us;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void logout() {
		try {
			user = null;
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
			FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
			FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// ACCESORES PARA JSF
	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUser_password() {
		return user_password;
	}

	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}

	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}

	// ACCESORES PARA SPRING
	public void setMgmt_user(IMgmt_User mgmt_user) {
		this.mgmt_user = mgmt_user;
	}

	public IMgmt_User getMgmt_user() {
		return mgmt_user;
	}
}