package com.formula.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.formula.functions.Util;
import com.formula.hibernate.Brand;
import com.formula.hibernate.model.IMgmt_Brand;

@ManagedBean(name="brand_bean")
@ViewScoped
public class BrandBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Brand selectedBrand = new Brand();
    private List<Brand> lstBrand;
    private String action;
	@ManagedProperty(value="#{mgmt_brand}")
	private IMgmt_Brand mgmt_brand;

	public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.clear();
        this.action = action;
    }

    public List<Brand> getLstBrand() {
        return lstBrand;
    }

    public void setLstBrand(List<Brand> lstBrand) {
        this.lstBrand = lstBrand;
    }

    public Brand getSelectedBrand() {
        return selectedBrand;
    }

    public void setSelectedBrand(Brand selectedBrand) {
        this.selectedBrand = selectedBrand;
    }
    
    public IMgmt_Brand getMgmt_brand() {
		return mgmt_brand;
	}

	public void setMgmt_brand(IMgmt_Brand mgmt_brand) {
		this.mgmt_brand = mgmt_brand;
	}

	private boolean isPostBack(){
        boolean answer;
        answer = FacesContext.getCurrentInstance().isPostback();
        return answer;
    }
    
    public void perform() throws Exception{
        switch(action){
            case "Create":
                this.create();
                this.clear();
                break;
            case "Update":
                this.update();
                this.clear();
                break;
        }
    }
    
    public void clear(){
    	if(selectedBrand==null) {
    		selectedBrand = new Brand();
    	}
        this.selectedBrand.setBrand_id(0L);
        this.selectedBrand.setBrand_name("");
    }
    
    private void create() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.brand.create.message");
		
        try {
        	mgmt_brand.create_Brand(selectedBrand);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
    
    public void reading(String value) throws Exception{
        try {
            if(value.equals("F")){
                if(isPostBack() == false){
                    lstBrand = mgmt_brand.reading_All();
                }
            } else {
                    lstBrand = mgmt_brand.reading_All();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void update() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.brand.edit.message");
		
        try {
        	mgmt_brand.update_Brand(selectedBrand);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }

    public void delete() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.brand.delete.message");
		
        try {
        	mgmt_brand.delete_Brand(selectedBrand);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
}