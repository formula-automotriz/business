package com.formula.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import com.formula.hibernate.Brand;
import com.formula.hibernate.Family;
import com.formula.hibernate.Line;
import com.formula.hibernate.Product;
import com.formula.hibernate.model.IMgmt_Brand;
import com.formula.hibernate.model.IMgmt_Family;
import com.formula.hibernate.model.IMgmt_Line;
import com.formula.hibernate.model.IMgmt_Product;

@ManagedBean(name="report_product_bean")
@ViewScoped
public class ReportProductBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Brand brand = new Brand();
	private Family family = new Family();
	private Line line = new Line();
	private Product selectedProduct = new Product();

    private List<Brand> lstBrand;
    private List<Family> lstFamily;
    private List<Line> lstLine;
    private List<Product> lstProduct;
	
	@ManagedProperty(value="#{mgmt_brand}")
	private IMgmt_Brand mgmt_brand;
	
	@ManagedProperty(value="#{mgmt_family}")
	private IMgmt_Family mgmt_family;

	@ManagedProperty(value="#{mgmt_line}")
	private IMgmt_Line mgmt_line;
	
	@ManagedProperty(value="#{mgmt_product}")
	private IMgmt_Product mgmt_product;

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public Family getFamily() {
		return family;
	}

	public void setFamily(Family family) {
		this.family = family;
	}

	public Line getLine() {
		return line;
	}

	public void setLine(Line line) {
		this.line = line;
	}

	public Product getSelectedProduct() {
		return selectedProduct;
	}

	public void setSelectedProduct(Product selectedProduct) {
		this.selectedProduct = selectedProduct;
	}

	public List<Brand> getLstBrand() {
		return lstBrand;
	}

	public List<Family> getLstFamily() {
		return lstFamily;
	}

	public List<Line> getLstLine() {
		return lstLine;
	}

	public List<Product> getLstProduct() {
		return lstProduct;
	}

	public void setMgmt_brand(IMgmt_Brand mgmt_brand) {
		this.mgmt_brand = mgmt_brand;
	}

	public void setMgmt_family(IMgmt_Family mgmt_family) {
		this.mgmt_family = mgmt_family;
	}

	public void setMgmt_line(IMgmt_Line mgmt_line) {
		this.mgmt_line = mgmt_line;
	}

	public void setMgmt_product(IMgmt_Product mgmt_product) {
		this.mgmt_product = mgmt_product;
	}

	private boolean isPostBack(){
        boolean answer;
        answer = FacesContext.getCurrentInstance().isPostback();
        return answer;
    }
	
    public void reading(String value) throws Exception{
        try {
            if(value.equals("F")){
                if(isPostBack() == false){
                    lstBrand = mgmt_brand.reading_All();
                    lstFamily = mgmt_family.reading_All();
                    lstLine = mgmt_line.reading_All();
                    lstProduct = mgmt_product.reading_All();
                }
            } else {
                    lstBrand = mgmt_brand.reading_All();
                    lstFamily = mgmt_family.reading_All();
                    lstLine = mgmt_line.reading_All();
                    lstProduct = mgmt_product.reading_All();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void brandChanged(ValueChangeEvent e) { 
    	  
    	brand = (Brand)e.getNewValue();
    	if(brand != null) {
            lstLine = mgmt_line.reading_ByProperty("brand.brand_id", brand.getBrand_id());
            lstProduct = mgmt_product.reading_ByProperty("line.brand.brand_id", brand.getBrand_id());
    	}
    	else {
    		lstProduct = mgmt_product.reading_All();
    	}
        family = null;
        line = null;
    }

    public void familyChanged(ValueChangeEvent e) { 
    	  
    	family = (Family)e.getNewValue();
    	if(family != null) {
            lstLine = mgmt_line.reading_ByProperty("family.family_id", family.getFamily_id());
            lstProduct = mgmt_product.reading_ByProperty("line.family.family_id", family.getFamily_id());
    	}
    	else {
    		lstProduct = mgmt_product.reading_All();
    	}
        brand = null;
        line = null;
    }

    public void lineChanged(ValueChangeEvent e) { 
    	  
    	line = (Line)e.getNewValue();
    	if(line != null) {
        	brand=line.getBrand();
        	family=line.getFamily();
            lstProduct = mgmt_product.reading_ByProperty("line.line_id", line.getLine_id());
    	}
    	else {
    		brand = null;
    		family = null;
    		lstLine = mgmt_line.reading_All();
    		lstProduct = mgmt_product.reading_All();
    	}
    }
}
