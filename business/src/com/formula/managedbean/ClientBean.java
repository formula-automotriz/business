package com.formula.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.formula.functions.Util;
import com.formula.hibernate.Client;
import com.formula.hibernate.model.IMgmt_Client;

@ManagedBean(name="client_bean")
@ViewScoped
public class ClientBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Client selectedClient = new Client();
    private List<Client> lstClient;
    private String action;
	@ManagedProperty(value="#{mgmt_client}")
	private IMgmt_Client mgmt_client;

	public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.clear();
        this.action = action;
    }

    public List<Client> getLstClient() {
        return lstClient;
    }

    public void setLstClient(List<Client> lstClient) {
        this.lstClient = lstClient;
    }

    public Client getSelectedClient() {
        return selectedClient;
    }

    public void setSelectedClient(Client selectedClient) {
        this.selectedClient = selectedClient;
    }
    
    public IMgmt_Client getMgmt_client() {
		return mgmt_client;
	}

	public void setMgmt_client(IMgmt_Client mgmt_client) {
		this.mgmt_client = mgmt_client;
	}

	private boolean isPostBack(){
        boolean answer;
        answer = FacesContext.getCurrentInstance().isPostback();
        return answer;
    }
    
    public void perform() throws Exception{
        switch(action){
            case "Create":
                this.create();
                this.clear();
                break;
            case "Update":
                this.update();
                this.clear();
                break;
        }
    }
    
    public void clear(){
    	if(selectedClient==null) {
    		selectedClient = new Client();
    	}
        this.selectedClient.setClient_id(0L);
        this.selectedClient.setClient_name("");
    }
    
    private void create() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.client.create.message");
		
        try {
        	mgmt_client.create_Client(selectedClient);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
    
    public void reading(String value) throws Exception{
        try {
            if(value.equals("F")){
                if(isPostBack() == false){
                    lstClient = mgmt_client.reading_All();
                }
            } else {
                    lstClient = mgmt_client.reading_All();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void update() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.client.edit.message");
		
        try {
        	mgmt_client.update_Client(selectedClient);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }

    public void delete() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.client.delete.message");
		
        try {
        	mgmt_client.delete_Client(selectedClient);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
}