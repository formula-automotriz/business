package com.formula.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.formula.functions.Util;
import com.formula.hibernate.Store;
import com.formula.hibernate.model.IMgmt_Store;

@ManagedBean(name="store_bean")
@ViewScoped
public class StoreBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Store selectedStore = new Store();
    private List<Store> lstStore;
    private String action;
	@ManagedProperty(value="#{mgmt_store}")
	private IMgmt_Store mgmt_store;

	public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.clear();
        this.action = action;
    }

    public List<Store> getLstStore() {
        return lstStore;
    }

    public void setLstStore(List<Store> lstStore) {
        this.lstStore = lstStore;
    }

    public Store getSelectedStore() {
        return selectedStore;
    }

    public void setSelectedStore(Store selectedStore) {
        this.selectedStore = selectedStore;
    }
    
    public IMgmt_Store getMgmt_store() {
		return mgmt_store;
	}

	public void setMgmt_store(IMgmt_Store mgmt_store) {
		this.mgmt_store = mgmt_store;
	}

	private boolean isPostBack(){
        boolean answer;
        answer = FacesContext.getCurrentInstance().isPostback();
        return answer;
    }
    
    public void perform() throws Exception{
        switch(action){
            case "Create":
                this.create();
                this.clear();
                break;
            case "Update":
                this.update();
                this.clear();
                break;
        }
    }
    
    public void clear(){
    	if(selectedStore==null) {
    		selectedStore = new Store();
    	}
        this.selectedStore.setStore_id(0L);
        this.selectedStore.setStore_name("");
    }
    
    private void create() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.store.create.message");
		
        try {
        	mgmt_store.create_Store(selectedStore);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
    
    public void reading(String value) throws Exception{
        try {
            if(value.equals("F")){
                if(isPostBack() == false){
                    lstStore = mgmt_store.reading_All();
                }
            } else {
                    lstStore = mgmt_store.reading_All();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void update() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.store.edit.message");
		
        try {
        	mgmt_store.update_Store(selectedStore);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }

    public void delete() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.store.delete.message");
		
        try {
        	mgmt_store.delete_Store(selectedStore);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
}