package com.formula.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.formula.functions.Util;
import com.formula.hibernate.Document;
import com.formula.hibernate.model.IMgmt_Document;

@ManagedBean(name="document_bean")
@ViewScoped
public class DocumentBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Document selectedDocument = new Document();
    private List<Document> lstDocument;
    private String action;
	@ManagedProperty(value="#{mgmt_document}")
	private IMgmt_Document mgmt_document;

	public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.clear();
        this.action = action;
    }

    public List<Document> getLstDocument() {
        return lstDocument;
    }

    public void setLstDocument(List<Document> lstDocument) {
        this.lstDocument = lstDocument;
    }

    public Document getSelectedDocument() {
        return selectedDocument;
    }

    public void setSelectedDocument(Document selectedDocument) {
        this.selectedDocument = selectedDocument;
    }
    
    public IMgmt_Document getMgmt_document() {
		return mgmt_document;
	}

	public void setMgmt_document(IMgmt_Document mgmt_document) {
		this.mgmt_document = mgmt_document;
	}

	private boolean isPostBack(){
        boolean answer;
        answer = FacesContext.getCurrentInstance().isPostback();
        return answer;
    }
    
    public void perform() throws Exception{
        switch(action){
            case "Create":
                this.create();
                this.clear();
                break;
            case "Update":
                this.update();
                this.clear();
                break;
        }
    }
    
    public void clear(){
    	if(selectedDocument==null) {
    		selectedDocument = new Document();
    	}
        this.selectedDocument.setDocument_id(0L);
        this.selectedDocument.setDocument_name("");
        this.selectedDocument.setDocument_code("");
    }
    
    private void create() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.document.create.message");
		
        try {
        	mgmt_document.create_Document(selectedDocument);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
    
    public void reading(String value) throws Exception{
        try {
            if(value.equals("F")){
                if(isPostBack() == false){
                    lstDocument = mgmt_document.reading_All();
                }
            } else {
                    lstDocument = mgmt_document.reading_All();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void update() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.document.edit.message");
		
        try {
        	mgmt_document.update_Document(selectedDocument);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }

    public void delete() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.document.delete.message");
		
        try {
        	mgmt_document.delete_Document(selectedDocument);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
}