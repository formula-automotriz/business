package com.formula.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.formula.functions.Util;
import com.formula.hibernate.Price;
import com.formula.hibernate.model.IMgmt_Price;

@ManagedBean(name="price_bean")
@ViewScoped
public class PriceBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Price selectedPrice = new Price();
    private List<Price> lstPrice;
    private String action;
	@ManagedProperty(value="#{mgmt_price}")
	private IMgmt_Price mgmt_price;

	public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.clear();
        this.action = action;
    }

    public List<Price> getLstPrice() {
        return lstPrice;
    }

    public void setLstPrice(List<Price> lstPrice) {
        this.lstPrice = lstPrice;
    }

    public Price getSelectedPrice() {
        return selectedPrice;
    }

    public void setSelectedPrice(Price selectedPrice) {
        this.selectedPrice = selectedPrice;
    }
    
    public IMgmt_Price getMgmt_price() {
		return mgmt_price;
	}

	public void setMgmt_price(IMgmt_Price mgmt_price) {
		this.mgmt_price = mgmt_price;
	}

	private boolean isPostBack(){
        boolean answer;
        answer = FacesContext.getCurrentInstance().isPostback();
        return answer;
    }
    
    public void perform() throws Exception{
        switch(action){
            case "Create":
                this.create();
                this.clear();
                break;
            case "Update":
                this.update();
                this.clear();
                break;
        }
    }
    
    public void clear(){
    	if(selectedPrice==null) {
    		selectedPrice = new Price();
    	}
        this.selectedPrice.setPrice_id(0L);;
        this.selectedPrice.setCost(0D);
        this.selectedPrice.setProfit(0D);
    }

    private void create() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.price.create.message");
		
        try {
        	mgmt_price.create_Price(selectedPrice);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
    
    public void reading(String value) throws Exception{
        try {
            if(value.equals("F")){
                if(isPostBack() == false){
                    lstPrice = mgmt_price.reading_All();
                }
            } else {
                    lstPrice = mgmt_price.reading_All();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void update() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.price.edit.message");
		
        try {
        	mgmt_price.update_Price(selectedPrice);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }

    public void delete() throws Exception{
		String error = Util.getKey("error.message.title");
		String error_message = Util.getKey("error.message.description");
		String info = Util.getKey("info.message.title");
		String info_message = Util.getKey("code.price.delete.message");
		
        try {
        	mgmt_price.delete_Price(selectedPrice);
            this.reading("T");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,info,info_message));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,error,error_message));
        }
    }
}