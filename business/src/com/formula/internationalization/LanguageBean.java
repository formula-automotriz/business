package com.formula.internationalization;

import java.io.Serializable;
import java.util.Locale;
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
  
@ManagedBean(name="language_bean")
@ViewScoped
public class LanguageBean implements Serializable{
     
    private static final long serialVersionUID = 1L;
     
    private Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
    private String language;
 
    public String getLanguage() {
    	language = locale.getLanguage();
        return language;
    }
    
    public void setLanguage(String language) {
        locale = new Locale(language);
        FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
    	this.language = locale.getLanguage();
    }
}