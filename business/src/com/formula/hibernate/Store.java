package com.formula.hibernate;

import java.io.Serializable;
//import java.util.Set;
//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
//import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "store")
@Table(name = "STORE")
public class Store implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long store_id;
	private String store_name;
	//private Set<Line> lines;
	
	public Store() {
		
	}

	public Store(Long store_id, String store_name/*, Set<Line> lines*/) {
		this.store_id = store_id;
		this.store_name = store_name;
		//this.lines = lines;
	}

	@Id
	@Column(name = "store_id", unique = true, nullable = false, precision = 11, scale = 0)
	public Long getStore_id() {
		return store_id;
	}

	public void setStore_id(Long store_id) {
		this.store_id = store_id;
	}

	@Column(name = "store_name", length = 50)
	public String getStore_name() {
		return store_name;
	}

	public void setStore_name(String store_name) {
		this.store_name = store_name;
	}

	//@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, mappedBy = "store")
	//public Set<Line> getLines() {
	//	return lines;
	//}

	//public void setLines(Set<Line> lines) {
	//	this.lines = lines;
	//}

	@Override
	public String toString() {
		//return "Store [store_id=" + store_id + ", store_name=" + store_name + "]";
		return String.format("%s[store_id=%d]", getClass().getSimpleName(),getStore_id());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((store_id == null) ? 0 : store_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Store other = (Store) obj;
		if (store_id == null) {
			if (other.store_id != null)
				return false;
		} else if (!store_id.equals(other.store_id))
			return false;
		return true;
	}
}