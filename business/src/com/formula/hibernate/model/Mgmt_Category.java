package com.formula.hibernate.model;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.formula.hibernate.Category;
import com.formula.hibernate.dao.CategoryDAO;

@Component("mgmt_category")
@Scope("prototype")
public class Mgmt_Category implements IMgmt_Category, java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private CategoryDAO category_dao;

	// ***************** CATEGORY QUERIES
	@Override
	@Transactional(readOnly = true)
	public List<Category> reading_All() {
		List<Category> list = category_dao.findAll();
		return list;
	}

	// *************** CRUD OPERATIONS: CATEGORY

	@Override
	@Transactional
	public void create_Category(Category category) {
		category_dao.save(category);
	}

	@Override
	@Transactional(readOnly = true)
	public Category reading_ById(Long category_id) {
		return category_dao.findById(category_id);
	}

	@Override
	@Transactional
	public void update_Category(Category category) {
		category_dao.attachDirty(category);
	}

	@Override
	@Transactional
	public void delete_Category(Category category) {
		category_dao.delete(category);
	}

	// ACCESORS FOR SPRING
	public void setCategory_dao(CategoryDAO category_dao) {
		this.category_dao = category_dao;
	}
}