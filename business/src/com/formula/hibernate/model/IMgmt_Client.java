package com.formula.hibernate.model;

import java.util.List;

import com.formula.hibernate.Client;

public interface IMgmt_Client {

	public abstract void create_Client(Client client);

	public Client reading_ById(Long client_id);

	public abstract void update_Client(Client client);

	public abstract void delete_Client(Client client);

	public abstract List<Client> reading_All();

}