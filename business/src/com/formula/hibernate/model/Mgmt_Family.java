package com.formula.hibernate.model;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.formula.hibernate.Family;
import com.formula.hibernate.dao.FamilyDAO;

@Component("mgmt_family")
@Scope("prototype")
public class Mgmt_Family implements IMgmt_Family, java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private FamilyDAO family_dao;

	// ***************** FAMILY QUERIES
	@Override
	@Transactional(readOnly = true)
	public List<Family> reading_All() {
		List<Family> list = family_dao.findAll();
		return list;
	}

	// *************** CRUD OPERATIONS: FAMILY

	@Override
	@Transactional
	public void create_Family(Family family) {
		family_dao.save(family);
	}

	@Override
	@Transactional(readOnly = true)
	public Family reading_ById(Long family_id) {
		return family_dao.findById(family_id);
	}

	@Override
	@Transactional
	public void update_Family(Family family) {
		family_dao.attachDirty(family);
	}

	@Override
	@Transactional
	public void delete_Family(Family family) {
		family_dao.delete(family);
	}

	// ACCESORS FOR SPRING
	public void setFamily_dao(FamilyDAO family_dao) {
		this.family_dao = family_dao;
	}
}