package com.formula.hibernate.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.formula.hibernate.Sale_Order_Detail;
import com.formula.hibernate.dao.Sale_Order_DetailDAO;

@Component("mgmt_sale_order_detail")
@Scope("prototype")
public class Mgmt_Sale_Order_Detail implements IMgmt_Sale_Order_Detail, Serializable {

	private static final long serialVersionUID = 1L;
	private Sale_Order_DetailDAO sale_order_detail_dao;

	// ***************** BRAND QUERIES
	@Override
	@Transactional(readOnly = true)
	public List<Sale_Order_Detail> reading_All() {
		List<Sale_Order_Detail> list = sale_order_detail_dao.findAll();
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public Long getMax() {
		return sale_order_detail_dao.getMax();
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Sale_Order_Detail> reading_List(Long order_id){
		List<Sale_Order_Detail> list = sale_order_detail_dao.reading_List(order_id);
		return list;
	}

	// *************** CRUD OPERATIONS: BRAND

	@Override
	@Transactional
	public void create_Sale_Order_Detail(Sale_Order_Detail sale_order_detail) {
		sale_order_detail_dao.save(sale_order_detail);
	}

	@Override
	@Transactional(readOnly = true)
	public Sale_Order_Detail reading_ById(Long detail_id) {
		return sale_order_detail_dao.findById(detail_id);
	}

	@Override
	@Transactional
	public void delete_Sale_Order_Detail(Sale_Order_Detail sale_order_detail) {
		sale_order_detail_dao.delete(sale_order_detail);
	}

	@Override
	@Transactional
	public void update_Sale_Order_Detail(Sale_Order_Detail sale_order_detail) {
		sale_order_detail_dao.attachDirty(sale_order_detail);
	}

	// ACCESORS FOR SPRING
	public void setSale_order_detail_dao(Sale_Order_DetailDAO sale_order_detail_dao) {
		this.sale_order_detail_dao = sale_order_detail_dao;
	}
}