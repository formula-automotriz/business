package com.formula.hibernate.model;

import java.util.List;

import com.formula.hibernate.Line;

public interface IMgmt_Line {

	public abstract void create_Line(Line line);

	public Line reading_ById(Long line_id);

	public abstract void update_Line(Line line);

	public abstract void delete_Line(Line line);

	public abstract List<Line> reading_All();
	
	public abstract List<Line> reading_ByProperty(String property, Long id);

}