package com.formula.hibernate.model;

import java.util.List;

import com.formula.hibernate.Metric;

public interface IMgmt_Metric {

	public abstract void create_Metric(Metric metric);

	public Metric reading_ById(Long metric_id);

	public abstract void update_Metric(Metric metric);

	public abstract void delete_Metric(Metric metric);

	public abstract List<Metric> reading_All();

	public abstract List<Metric> reading_ByProperty(String property, Long id);

}
