package com.formula.hibernate.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.formula.hibernate.Client;
import com.formula.hibernate.dao.ClientDAO;

@Component("mgmt_client")
@Scope("prototype")
public class Mgmt_Client implements IMgmt_Client, Serializable {

	private static final long serialVersionUID = 1L;
	private ClientDAO client_dao;

	// ***************** BRAND QUERIES
	@Override
	@Transactional(readOnly = true)
	public List<Client> reading_All() {
		List<Client> list = client_dao.findAll();
		return list;
	}

	// *************** CRUD OPERATIONS: BRAND

	@Override
	@Transactional
	public void create_Client(Client client) {
		client_dao.save(client);
	}

	@Override
	@Transactional(readOnly = true)
	public Client reading_ById(Long client_id) {
		return client_dao.findById(client_id);
	}

	@Override
	@Transactional
	public void delete_Client(Client client) {
		client_dao.delete(client);
	}

	@Override
	@Transactional
	public void update_Client(Client client) {
		client_dao.attachDirty(client);
	}

	// ACCESORS FOR SPRING
	public void setClient_dao(ClientDAO client_dao) {
		this.client_dao = client_dao;
	}
}