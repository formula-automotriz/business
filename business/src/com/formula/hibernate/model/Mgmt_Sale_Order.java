package com.formula.hibernate.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.formula.hibernate.Sale_Order;
import com.formula.hibernate.dao.Sale_OrderDAO;

@Component("mgmt_sale_order")
@Scope("prototype")
public class Mgmt_Sale_Order implements IMgmt_Sale_Order, Serializable {

	private static final long serialVersionUID = 1L;
	private Sale_OrderDAO sale_order_dao;

	// ***************** BRAND QUERIES
	@Override
	@Transactional(readOnly = true)
	public List<Sale_Order> reading_All() {
		List<Sale_Order> list = sale_order_dao.findAll();
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public Long getMax() {
		return sale_order_dao.getMax();
	}

	// *************** CRUD OPERATIONS: BRAND

	@Override
	@Transactional
	public void create_Sale_Order(Sale_Order sale_order) {
		sale_order_dao.save(sale_order);
	}

	@Override
	@Transactional(readOnly = true)
	public Sale_Order reading_ById(Long order_id) {
		return sale_order_dao.findById(order_id);
	}

	@Override
	@Transactional
	public void delete_Sale_Order(Sale_Order sale_order) {
		sale_order_dao.delete(sale_order);
	}

	@Override
	@Transactional
	public void update_Sale_Order(Sale_Order sale_order) {
		sale_order_dao.attachDirty(sale_order);
	}

	// ACCESORS FOR SPRING
	public void setSale_order_dao(Sale_OrderDAO sale_order_dao) {
		this.sale_order_dao = sale_order_dao;
	}
}