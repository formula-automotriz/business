package com.formula.hibernate.model;

import java.util.List;

import com.formula.hibernate.Brand;

public interface IMgmt_Brand {

	public abstract void create_Brand(Brand brand);

	public Brand reading_ById(Long brand_id);

	public abstract void update_Brand(Brand brand);

	public abstract void delete_Brand(Brand brand);

	public abstract List<Brand> reading_All();

}