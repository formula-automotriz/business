package com.formula.hibernate.model;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.formula.hibernate.Line;
import com.formula.hibernate.dao.LineDAO;

@Component("mgmt_line")
@Scope("prototype")
public class Mgmt_Line implements IMgmt_Line, java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private LineDAO line_dao;

	// ***************** LINE QUERIES
	@Override
	@Transactional(readOnly = true)
	public List<Line> reading_All() {
		List<Line> list = line_dao.findAll();
		return list;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Line> reading_ByProperty(String property, Long id){
		List<Line> list = line_dao.findByProperty(property, id);
		return list;
	}

	// *************** CRUD OPERATIONS: LINE

	@Override
	@Transactional
	public void create_Line(Line line) {
		line_dao.save(line);
	}

	@Override
	@Transactional(readOnly = true)
	public Line reading_ById(Long line_id) {
		return line_dao.findById(line_id);
	}

	@Override
	@Transactional
	public void update_Line(Line line) {
		line_dao.attachDirty(line);
	}

	@Override
	@Transactional
	public void delete_Line(Line line) {
		line_dao.delete(line);
	}

	// ACCESORS FOR SPRING
	public void setLine_dao(LineDAO line_dao) {
		this.line_dao = line_dao;
	}
}