package com.formula.hibernate.model;

import java.util.List;

import com.formula.hibernate.Magnitude;

public interface IMgmt_Magnitude {

	public abstract void create_Magnitude(Magnitude magnitude);

	public Magnitude reading_ById(Long magnitude_id);

	public abstract void update_Magnitude(Magnitude magnitude);

	public abstract void delete_Magnitude(Magnitude magnitude);

	public abstract List<Magnitude> reading_All();

}
