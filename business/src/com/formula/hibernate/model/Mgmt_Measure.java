package com.formula.hibernate.model;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.formula.hibernate.Measure;
import com.formula.hibernate.dao.MeasureDAO;

@Component("mgmt_measure")
@Scope("prototype")
public class Mgmt_Measure implements IMgmt_Measure, java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private MeasureDAO measure_dao;

	// ***************** MEASURE QUERIES
	@Override
	@Transactional(readOnly = true)
	public List<Measure> reading_All() {
		List<Measure> list = measure_dao.findAll();
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Measure> reading_ByProperty(String property, Long id){
		List<Measure> list = measure_dao.findByProperty(property, id);
		return list;
	}

	// *************** CRUD OPERATIONS: MEASURE

	@Override
	@Transactional
	public void create_Measure(Measure measure) {
		measure_dao.save(measure);
	}

	@Override
	@Transactional(readOnly = true)
	public Measure reading_ById(Long measure_id) {
		return measure_dao.findById(measure_id);
	}

	@Override
	@Transactional
	public void update_Measure(Measure measure) {
		measure_dao.attachDirty(measure);
	}

	@Override
	@Transactional
	public void delete_Measure(Measure measure) {
		measure_dao.delete(measure);
	}

	// ACCESORS FOR SPRING
	public void setMeasure_dao(MeasureDAO measure_dao) {
		this.measure_dao = measure_dao;
	}
}