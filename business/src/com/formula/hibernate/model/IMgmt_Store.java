package com.formula.hibernate.model;

import java.util.List;

import com.formula.hibernate.Store;

public interface IMgmt_Store {

	public abstract void create_Store(Store store);

	public Store reading_ById(Long store_id);

	public abstract void update_Store(Store store);

	public abstract void delete_Store(Store store);

	public abstract List<Store> reading_All();

}