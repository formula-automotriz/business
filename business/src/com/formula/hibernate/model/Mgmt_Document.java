package com.formula.hibernate.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.formula.hibernate.Document;
import com.formula.hibernate.dao.DocumentDAO;

@Component("mgmt_document")
@Scope("prototype")
public class Mgmt_Document implements IMgmt_Document, Serializable {

	private static final long serialVersionUID = 1L;
	private DocumentDAO document_dao;

	// ***************** DOCUMENT QUERIES
	@Override
	@Transactional(readOnly = true)
	public List<Document> reading_All() {
		List<Document> list = document_dao.findAll();
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public Document reading_ByCode(String document_code) {
		return document_dao.findByCode(document_code);
	}

	// *************** CRUD OPERATIONS: DOCUMENT

	@Override
	@Transactional
	public void create_Document(Document document) {
		document_dao.save(document);
	}

	@Override
	@Transactional(readOnly = true)
	public Document reading_ById(Long document_id) {
		return document_dao.findById(document_id);
	}

	@Override
	@Transactional
	public void delete_Document(Document document) {
		document_dao.delete(document);
	}

	@Override
	@Transactional
	public void update_Document(Document document) {
		document_dao.attachDirty(document);
	}

	// ACCESORS FOR SPRING
	public void setDocument_dao(DocumentDAO document_dao) {
		this.document_dao = document_dao;
	}
}