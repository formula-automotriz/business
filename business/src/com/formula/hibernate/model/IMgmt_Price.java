package com.formula.hibernate.model;

import java.util.Date;
import java.util.List;

import com.formula.hibernate.Price;

public interface IMgmt_Price {

	public abstract void create_Price(Price price);

	public Price reading_ById(Long price_id);

	public abstract void update_Price(Price price);

	public abstract void delete_Price(Price price);

	public abstract List<Price> reading_All();
	
	public abstract List<Price> reading_ByProperty(String property, Long id);

	public abstract Long getMax();
	
	public abstract List<Price> reading_ByProductDate(Object product, Date price_date);

}