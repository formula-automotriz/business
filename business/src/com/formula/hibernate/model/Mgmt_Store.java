package com.formula.hibernate.model;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.formula.hibernate.Store;
import com.formula.hibernate.dao.StoreDAO;

@Component("mgmt_store")
@Scope("prototype")
public class Mgmt_Store implements IMgmt_Store, java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private StoreDAO store_dao;

	// ***************** BRAND QUERIES
	@Override
	@Transactional(readOnly = true)
	public List<Store> reading_All() {
		List<Store> list = store_dao.findAll();
		return list;
	}

	// *************** CRUD OPERATIONS: BRAND

	@Override
	@Transactional
	public void create_Store(Store store) {
		store_dao.save(store);
	}

	@Override
	@Transactional(readOnly = true)
	public Store reading_ById(Long store_id) {
		return store_dao.findById(store_id);
	}

	@Override
	@Transactional
	public void delete_Store(Store store) {
		store_dao.delete(store);
	}

	@Override
	@Transactional
	public void update_Store(Store store) {
		store_dao.attachDirty(store);
	}

	// ACCESORS FOR SPRING
	public void setStore_dao(StoreDAO store_dao) {
		this.store_dao = store_dao;
	}
}