package com.formula.hibernate.model;

import java.util.List;

import com.formula.hibernate.Category;

public interface IMgmt_Category {

	public abstract void create_Category(Category category);

	public Category reading_ById(Long category_id);

	public abstract void update_Category(Category category);

	public abstract void delete_Category(Category category);

	public abstract List<Category> reading_All();

}