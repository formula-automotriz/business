package com.formula.hibernate.model;

import java.util.List;

import com.formula.hibernate.Sale_Order_Detail;

public interface IMgmt_Sale_Order_Detail {

	public abstract void create_Sale_Order_Detail(Sale_Order_Detail sale_order_detail);

	public Sale_Order_Detail reading_ById(Long detail_id);

	public abstract void update_Sale_Order_Detail(Sale_Order_Detail sale_order_detail);

	public abstract void delete_Sale_Order_Detail(Sale_Order_Detail sale_order_detail);

	public abstract List<Sale_Order_Detail> reading_All();
	
	public abstract Long getMax();
	
	public abstract List<Sale_Order_Detail> reading_List(Long order_id);

}