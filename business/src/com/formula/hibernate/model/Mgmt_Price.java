package com.formula.hibernate.model;

import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.formula.hibernate.Price;
import com.formula.hibernate.dao.PriceDAO;

@Component("mgmt_price")
@Scope("prototype")
public class Mgmt_Price implements IMgmt_Price, java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private PriceDAO price_dao;

	// ***************** PRICE QUERIES
	@Override
	@Transactional(readOnly = true)
	public List<Price> reading_All() {
		List<Price> list = price_dao.findAll();
		return list;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Price> reading_ByProperty(String property, Long id){
		List<Price> list = price_dao.findByProperty(property, id);
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public Long getMax() {
		return price_dao.getMax();
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Price> reading_ByProductDate(Object product, Date price_date){
		List<Price> list = price_dao.findByProductDate(product, price_date);
		return list;
	}

	// *************** CRUD OPERATIONS: PRICE

	@Override
	@Transactional
	public void create_Price(Price price) {
		price_dao.save(price);
	}

	@Override
	@Transactional(readOnly = true)
	public Price reading_ById(Long price_id) {
		return price_dao.findById(price_id);
	}

	@Override
	@Transactional
	public void update_Price(Price price) {
		price_dao.attachDirty(price);
	}

	@Override
	@Transactional
	public void delete_Price(Price price) {
		price_dao.delete(price);
	}

	// ACCESORS FOR SPRING
	public void setPrice_dao(PriceDAO price_dao) {
		this.price_dao = price_dao;
	}
}