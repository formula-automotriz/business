package com.formula.hibernate.model;

import java.util.List;

import com.formula.hibernate.Measure;

public interface IMgmt_Measure {

	public abstract void create_Measure(Measure measure);

	public Measure reading_ById(Long measure_id);

	public abstract void update_Measure(Measure measure);

	public abstract void delete_Measure(Measure measure);

	public abstract List<Measure> reading_All();

	public abstract List<Measure> reading_ByProperty(String property, Long id);

}