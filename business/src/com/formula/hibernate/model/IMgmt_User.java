package com.formula.hibernate.model;

import java.util.List;

import com.formula.hibernate.User;

public interface IMgmt_User {

	public abstract void create_User(User user);

	public User reading_ById(Long user_id);

	public abstract void update_User(User user);

	public abstract void delete_User(User user);

	public abstract List<User> reading_All();

	public User login(User user);

}