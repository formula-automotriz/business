package com.formula.hibernate.model;

import java.util.List;

import com.formula.hibernate.Document;

public interface IMgmt_Document {

	public abstract void create_Document(Document document);

	public Document reading_ById(Long document_id);

	public abstract void update_Document(Document document);

	public abstract void delete_Document(Document document);

	public abstract List<Document> reading_All();

	public Document reading_ByCode(String document_code);

}