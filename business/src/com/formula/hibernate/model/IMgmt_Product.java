package com.formula.hibernate.model;

import java.util.List;

import com.formula.hibernate.Product;

public interface IMgmt_Product {

	public abstract void create_Product(Product product);

	public Product reading_ById(Long product_id);

	public abstract void update_Product(Product product);

	public abstract void delete_Product(Product product);

	public abstract List<Product> reading_All();

	public abstract List<Product> reading_ByProperty(String property, Long id);

}