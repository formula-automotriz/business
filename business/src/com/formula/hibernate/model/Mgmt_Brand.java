package com.formula.hibernate.model;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.formula.hibernate.Brand;
import com.formula.hibernate.dao.BrandDAO;

@Component("mgmt_brand")
@Scope("prototype")
public class Mgmt_Brand implements IMgmt_Brand, java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private BrandDAO brand_dao;

	// ***************** BRAND QUERIES
	@Override
	@Transactional(readOnly = true)
	public List<Brand> reading_All() {
		List<Brand> list = brand_dao.findAll();
		return list;
	}

	// *************** CRUD OPERATIONS: BRAND

	@Override
	@Transactional
	public void create_Brand(Brand brand) {
		brand_dao.save(brand);
	}

	@Override
	@Transactional(readOnly = true)
	public Brand reading_ById(Long brand_id) {
		return brand_dao.findById(brand_id);
	}

	@Override
	@Transactional
	public void delete_Brand(Brand brand) {
		brand_dao.delete(brand);
	}

	@Override
	@Transactional
	public void update_Brand(Brand brand) {
		brand_dao.attachDirty(brand);
	}

	// ACCESORS FOR SPRING
	public void setBrand_dao(BrandDAO brand_dao) {
		this.brand_dao = brand_dao;
	}
}