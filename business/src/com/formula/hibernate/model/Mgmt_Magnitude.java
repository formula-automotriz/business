package com.formula.hibernate.model;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.formula.hibernate.Magnitude;
import com.formula.hibernate.dao.MagnitudeDAO;

@Component("mgmt_magnitude")
@Scope("prototype")
public class Mgmt_Magnitude implements IMgmt_Magnitude, java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private MagnitudeDAO magnitude_dao;

	// ***************** BRAND QUERIES
	@Override
	@Transactional(readOnly = true)
	public List<Magnitude> reading_All() {
		List<Magnitude> list = magnitude_dao.findAll();
		return list;
	}

	// *************** CRUD OPERATIONS: BRAND

	@Override
	@Transactional
	public void create_Magnitude(Magnitude magnitude) {
		magnitude_dao.save(magnitude);
	}

	@Override
	@Transactional(readOnly = true)
	public Magnitude reading_ById(Long magnitude_id) {
		return magnitude_dao.findById(magnitude_id);
	}

	@Override
	@Transactional
	public void delete_Magnitude(Magnitude magnitude) {
		magnitude_dao.delete(magnitude);
	}

	@Override
	@Transactional
	public void update_Magnitude(Magnitude magnitude) {
		magnitude_dao.attachDirty(magnitude);
	}

	// ACCESORS FOR SPRING
	public void setMagnitude_dao(MagnitudeDAO magnitude_dao) {
		this.magnitude_dao = magnitude_dao;
	}
}