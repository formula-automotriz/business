package com.formula.hibernate.model;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.formula.hibernate.Product;
import com.formula.hibernate.dao.ProductDAO;

@Component("mgmt_product")
@Scope("prototype")
public class Mgmt_Product implements IMgmt_Product, java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private ProductDAO product_dao;

	// ***************** PRODUCT QUERIES
	@Override
	@Transactional(readOnly = true)
	public List<Product> reading_All() {
		List<Product> list = product_dao.findAll();
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Product> reading_ByProperty(String property, Long id){
		List<Product> list = product_dao.findByProperty(property, id);
		return list;
	}
	// *************** CRUD OPERATIONS: PRODUCT

	@Override
	@Transactional
	public void create_Product(Product product) {
		product_dao.save(product);
	}

	@Override
	@Transactional(readOnly = true)
	public Product reading_ById(Long product_id) {
		return product_dao.findById(product_id);
	}

	@Override
	@Transactional
	public void update_Product(Product product) {
		product_dao.attachDirty(product);
	}

	@Override
	@Transactional
	public void delete_Product(Product product) {
		product_dao.delete(product);
	}

	// ACCESORS FOR SPRING
	public void setProduct_dao(ProductDAO product_dao) {
		this.product_dao = product_dao;
	}
}