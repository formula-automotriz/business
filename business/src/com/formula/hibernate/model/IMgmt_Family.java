package com.formula.hibernate.model;

import java.util.List;

import com.formula.hibernate.Family;

public interface IMgmt_Family {

	public abstract void create_Family(Family family);

	public Family reading_ById(Long family_id);

	public abstract void update_Family(Family family);

	public abstract void delete_Family(Family family);

	public abstract List<Family> reading_All();

}