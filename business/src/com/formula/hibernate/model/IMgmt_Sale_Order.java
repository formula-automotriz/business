package com.formula.hibernate.model;

import java.util.List;

import com.formula.hibernate.Sale_Order;

public interface IMgmt_Sale_Order {

	public abstract void create_Sale_Order(Sale_Order sale_order);

	public Sale_Order reading_ById(Long order_id);

	public abstract void update_Sale_Order(Sale_Order sale_order);

	public abstract void delete_Sale_Order(Sale_Order sale_order);

	public abstract List<Sale_Order> reading_All();
	
	public abstract Long getMax();

}