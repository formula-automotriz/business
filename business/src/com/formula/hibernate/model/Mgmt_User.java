package com.formula.hibernate.model;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.formula.hibernate.User;
import com.formula.hibernate.dao.UserDAO;

@Component("mgmt_user")
@Scope("prototype")
public class Mgmt_User implements IMgmt_User, java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private UserDAO user_dao;

	// ***************** BRAND QUERIES
	@Override
	@Transactional(readOnly = true)
	public List<User> reading_All() {
		List<User> list = user_dao.findAll();
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public User login(User user) {
		return user_dao.login(user);
	}
	// *************** CRUD OPERATIONS: BRAND

	@Override
	@Transactional
	public void create_User(User user) {
		user_dao.save(user);
	}

	@Override
	@Transactional(readOnly = true)
	public User reading_ById(Long user_id) {
		return user_dao.findById(user_id);
	}

	@Override
	@Transactional
	public void delete_User(User user) {
		user_dao.delete(user);
	}

	@Override
	@Transactional
	public void update_User(User user) {
		user_dao.attachDirty(user);
	}

	// ACCESORS FOR SPRING
	public void setUser_dao(UserDAO user_dao) {
		this.user_dao = user_dao;
	}
}