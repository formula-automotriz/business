package com.formula.hibernate.model;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.formula.hibernate.Metric;
import com.formula.hibernate.dao.MetricDAO;

@Component("mgmt_metric")
@Scope("prototype")
public class Mgmt_Metric implements IMgmt_Metric, java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private MetricDAO metric_dao;

	// ***************** METRIC QUERIES
	@Override
	@Transactional(readOnly = true)
	public List<Metric> reading_All() {
		List<Metric> list = metric_dao.findAll();
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Metric> reading_ByProperty(String property, Long id){
		List<Metric> list = metric_dao.findByProperty(property, id);
		return list;
	}
	// *************** CRUD OPERATIONS: METRIC

	@Override
	@Transactional
	public void create_Metric(Metric metric) {
		metric_dao.save(metric);
	}

	@Override
	@Transactional(readOnly = true)
	public Metric reading_ById(Long metric_id) {
		return metric_dao.findById(metric_id);
	}

	@Override
	@Transactional
	public void delete_Metric(Metric metric) {
		metric_dao.delete(metric);
	}

	@Override
	@Transactional
	public void update_Metric(Metric metric) {
		metric_dao.attachDirty(metric);
	}

	// ACCESORS FOR SPRING
	public void setMetric_dao(MetricDAO metric_dao) {
		this.metric_dao = metric_dao;
	}
}