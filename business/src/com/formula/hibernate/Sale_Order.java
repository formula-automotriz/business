package com.formula.hibernate;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "sale_order")
@Table(name = "SALE_ORDER")
public class Sale_Order implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long order_id;
	private Client client;
	private Date order_date;
	private Double order_subtotal;
	private Double order_tax;
	private Double order_discount;
	private Double order_total;
	private Double discounted_amount;
	private Double order_payment;
	private Set<Sale_Order_Detail> details;
	
	public Sale_Order() {
		
	}

	public Sale_Order(Long order_id, Client client, Date order_date, Double order_subtotal, Double order_tax,
			Double order_discount, Double order_total, Double discounted_amount, Double order_payment, Set<Sale_Order_Detail> details) {
		this.order_id = order_id;
		this.client = client;
		this.order_date = order_date;
		this.order_subtotal = order_subtotal;
		this.order_tax = order_tax;
		this.order_discount = order_discount;
		this.order_total = order_total;
		this.discounted_amount = discounted_amount;
		this.order_payment = order_payment;
		this.details = details;
	}

	@Id
	@Column(name = "order_id", unique = true, nullable = false, precision = 11, scale = 0)
	public Long getOrder_id() {
		return order_id;
	}

	public void setOrder_id(Long order_id) {
		this.order_id = order_id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "client_id")
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "order_date", length = 7)
	public Date getOrder_date() {
		return order_date;
	}

	public void setOrder_date(Date order_date) {
		this.order_date = order_date;
	}

	@Column(name = "order_subtotal", unique = false, nullable = false, precision = 11, scale = 2)
	public Double getOrder_subtotal() {
		return order_subtotal;
	}

	public void setOrder_subtotal(Double order_subtotal) {
		this.order_subtotal = order_subtotal;
	}

	@Column(name = "order_tax", unique = false, nullable = false, precision = 11, scale = 2)
	public Double getOrder_tax() {
		return order_tax;
	}

	public void setOrder_tax(Double order_tax) {
		this.order_tax = order_tax;
	}

	@Column(name = "order_discount", unique = false, nullable = false, precision = 11, scale = 2)
	public Double getOrder_discount() {
		return order_discount;
	}

	public void setOrder_discount(Double order_discount) {
		this.order_discount = order_discount;
	}

	@Column(name = "order_total", unique = false, nullable = false, precision = 11, scale = 2)
	public Double getOrder_total() {
		return order_total;
	}

	public void setOrder_total(Double order_total) {
		this.order_total = order_total;
	}

	@Column(name = "discounted_amount", unique = false, nullable = false, precision = 11, scale = 2)
	public Double getDiscounted_amount() {
		return discounted_amount;
	}

	public void setDiscounted_amount(Double discounted_amount) {
		this.discounted_amount = discounted_amount;
	}

	@Column(name = "order_payment", unique = false, nullable = false, precision = 11, scale = 2)
	public Double getOrder_payment() {
		return order_payment;
	}

	public void setOrder_payment(Double order_payment) {
		this.order_payment = order_payment;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, mappedBy = "sale_order")
	public Set<Sale_Order_Detail> getDetails() {
		return details;
	}

	public void setDetails(Set<Sale_Order_Detail> details) {
		this.details = details;
	}

	@Override
	public String toString() {
		//return "Order [order_id=" + order_id + ", order_name=" + order_name + "]";
		return String.format("%s[order_id=%d]", getClass().getSimpleName(),getOrder_id());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((order_id == null) ? 0 : order_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sale_Order other = (Sale_Order) obj;
		if (order_id == null) {
			if (other.order_id != null)
				return false;
		} else if (!order_id.equals(other.order_id))
			return false;
		return true;
	}
}
