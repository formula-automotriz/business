package com.formula.hibernate;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "line")
@Table(name = "LINE")
public class Line implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long line_id;
	private String line_name;
	private Brand brand;
	private Family family;
	private Set<Product> products;
	
	public Line() {
		
	}

	public Line(Long line_id, String line_name, Brand brand, Family family, Set<Product> products) {
		this.line_id = line_id;
		this.line_name = line_name;
		this.brand = brand;
		this.family = family;
		this.products = products;
	}

	@Id
	@Column(name = "line_id", unique = true, nullable = false, precision = 11, scale = 0)
	public Long getLine_id() {
		return line_id;
	}

	public void setLine_id(Long line_id) {
		this.line_id = line_id;
	}

	@Column(name = "line_name", length = 50)
	public String getLine_name() {
		return line_name;
	}

	public void setLine_name(String line_name) {
		this.line_name = line_name;
	}

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name = "brand_id")
	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name = "family_id")
	public Family getFamily() {
		return family;
	}

	public void setFamily(Family family) {
		this.family = family;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, mappedBy = "line")
	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	@Override
	public String toString() {
		//return "Line [line_id=" + line_id + ", line_name=" + line_name + "]";
		return String.format("%s[line_id=%d]", getClass().getSimpleName(),getLine_id());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((line_id == null) ? 0 : line_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Line other = (Line) obj;
		if (line_id == null) {
			if (other.line_id != null)
				return false;
		} else if (!line_id.equals(other.line_id))
			return false;
		return true;
	}
}
