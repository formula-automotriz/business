package com.formula.hibernate;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "magnitude")
@Table(name = "MAGNITUDE")
public class Magnitude implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long magnitude_id;
	private String magnitude_name;
	private Set<Metric> metrics;
	
	public Magnitude() {
		
	}

	public Magnitude(Long magnitude_id, String magnitude_name, Set<Metric> metrics) {
		this.magnitude_id = magnitude_id;
		this.magnitude_name = magnitude_name;
		this.metrics = metrics;
	}

	@Id
	@Column(name = "magnitude_id", unique = true, nullable = false, precision = 11, scale = 0)
	public Long getMagnitude_id() {
		return magnitude_id;
	}

	public void setMagnitude_id(Long magnitude_id) {
		this.magnitude_id = magnitude_id;
	}

	@Column(name = "magnitude_name", length = 50)
	public String getMagnitude_name() {
		return magnitude_name;
	}

	public void setMagnitude_name(String magnitude_name) {
		this.magnitude_name = magnitude_name;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, mappedBy = "magnitude")
	public Set<Metric> getMetrics() {
		return metrics;
	}

	public void setMetrics(Set<Metric> metrics) {
		this.metrics = metrics;
	}

	@Override
	public String toString() {
		//return "Magnitude [magnitude_id=" + magnitude_id + ", magnitude_name=" + magnitude_name + "]";
		return String.format("%s[magnitude_id=%d]", getClass().getSimpleName(),getMagnitude_id());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((magnitude_id == null) ? 0 : magnitude_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Magnitude other = (Magnitude) obj;
		if (magnitude_id == null) {
			if (other.magnitude_id != null)
				return false;
		} else if (!magnitude_id.equals(other.magnitude_id))
			return false;
		return true;
	}
}