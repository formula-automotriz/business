package com.formula.hibernate;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "metric")
@Table(name = "METRIC")
public class Metric implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long metric_id;
	private String metric_name;
	private String metric_code;
	private Magnitude magnitude;
	private Set<Measure> measures;
	
	public Metric() {
		
	}

	public Metric(Long metric_id, String metric_name, String metric_code, Magnitude magnitude, Set<Measure> measures) {
		this.metric_id = metric_id;
		this.metric_name = metric_name;
		this.metric_code = metric_code;
		this.magnitude = magnitude;
		this.measures = measures;
	}

	@Id
	@Column(name = "metric_id", unique = true, nullable = false, precision = 11, scale = 0)
	public Long getMetric_id() {
		return metric_id;
	}

	public void setMetric_id(Long metric_id) {
		this.metric_id = metric_id;
	}

	@Column(name = "metric_name", length = 50)
	public String getMetric_name() {
		return metric_name;
	}

	public void setMetric_name(String metric_name) {
		this.metric_name = metric_name;
	}

	public String getMetric_code() {
		return metric_code;
	}

	public void setMetric_code(String metric_code) {
		this.metric_code = metric_code;
	}

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name = "magnitude_id")
	public Magnitude getMagnitude() {
		return magnitude;
	}

	public void setMagnitude(Magnitude magnitude) {
		this.magnitude = magnitude;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, mappedBy = "metric")
	public Set<Measure> getMeasures() {
		return measures;
	}

	public void setMeasures(Set<Measure> measures) {
		this.measures = measures;
	}

	@Override
	public String toString() {
		//return "Metric [metric_id=" + metric_id + ", metric_name=" + metric_name + "]";
		return String.format("%s[metric_id=%d]", getClass().getSimpleName(),getMetric_id());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((metric_id == null) ? 0 : metric_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Metric other = (Metric) obj;
		if (metric_id == null) {
			if (other.metric_id != null)
				return false;
		} else if (!metric_id.equals(other.metric_id))
			return false;
		return true;
	}
}
