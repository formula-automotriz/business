package com.formula.hibernate;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "user")
@Table(name = "USER")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long user_id;
	private String user_name;
	private String user_password;
	private String first_name;
	private String last_name;

	public User() {
		
	}

	public User(Long user_id, String user_name, String user_password) {
		this.user_id = user_id;
		this.user_name = user_name;
		this.user_password = user_password;
	}

	@Id
	@Column(name = "user_id", unique = true, nullable = false, precision = 11, scale = 0)
	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	@Column(name = "user_name", length = 50)
	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	@Column(name = "user_password", length = 50)
	public String getUser_password() {
		return user_password;
	}

	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}

	@Column(name = "first_name", length = 50)
	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	@Column(name = "last_name", length = 50)
	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	@Override
	public String toString() {
		//return "User [user_id=" + user_id + ", user_name=" + user_name + "]";
		return String.format("%s[user_id=%d]", getClass().getSimpleName(),getUser_id());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((user_id == null) ? 0 : user_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (user_id == null) {
			if (other.user_id != null)
				return false;
		} else if (!user_id.equals(other.user_id))
			return false;
		return true;
	}
}