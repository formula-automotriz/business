package com.formula.hibernate.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import static org.hibernate.criterion.Example.create;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.formula.hibernate.Brand;

@Repository("brand_dao")
@Scope("prototype")
@SuppressWarnings({"unchecked"})
public class BrandDAO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(BrandDAO.class);
	// property constants
	public static final String BRAND_NAME = "brandName";

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected void initDao() {
		// do nothing
	}

	public void save(Brand transientInstance) {
		log.debug("saving Brands instance");
		try {
			getCurrentSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Brand persistentInstance) {
		log.debug("deleting Brands instance");
		try {
			getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Brand findById(java.lang.Long brand_id) {
		log.debug("getting Brands instance with brand_id: " + brand_id);
		try {
			Brand instance = (Brand) getCurrentSession().get(
					"com.formula.hibernate.Brand", brand_id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Brand> findByExample(Brand instance) {
		log.debug("finding Brands instance by example");
		try {
			List<Brand> results = (List<Brand>) getCurrentSession()
					.createCriteria("com.formula.hibernate.Brand")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List<Brand> findByProperty(String propertyName, Object value) {
		log.debug("finding Brands instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from brand as model where model."
					+ propertyName + "= :val";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter("val", value);
			return (List<Brand>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List<Brand> findByBrandName(Object brandName) {
		return findByProperty(BRAND_NAME, brandName);
	}

	public List<Brand> findAll() {
		log.debug("finding all Brands instances");
		try {
			String queryString = "from brand";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return (List<Brand>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Brand merge(Brand detachedInstance) {
		log.debug("merging Brands instance");
		try {
			Brand result = (Brand) getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Brand instance) {
		log.debug("attaching dirty Brands instance");
		try {
			getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Brand instance) {
		log.debug("attaching clean Brands instance");
		try {
			getCurrentSession().buildLockRequest(LockOptions.NONE).lock(
					instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static BrandDAO getFromApplicationContext(ApplicationContext ctx) {
		return (BrandDAO) ctx.getBean("BrandDAO");
	}
}