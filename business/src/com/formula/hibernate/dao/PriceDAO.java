package com.formula.hibernate.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import static org.hibernate.criterion.Example.create;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.formula.hibernate.Price;

@Repository("price_dao")
@Scope("prototype")
@SuppressWarnings({"unchecked"})
public class PriceDAO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(PriceDAO.class);

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected void initDao() {
		// do nothing
	}

	public Long getMax() {
		log.debug("getting the Max function");
		try {
			String queryString = "select max(p.price_id)as price_id from price p";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return (Long) queryObject.list().get(0);
		} catch (RuntimeException re) {
			log.error("get the max value failed", re);
			throw re;
		}
	}

	public void save(Price transientInstance) {
		log.debug("saving Prices instance");
		try {
			getCurrentSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Price persistentInstance) {
		log.debug("deleting Prices instance");
		try {
			getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Price findById(java.lang.Long price_id) {
		log.debug("getting Prices instance with price_id: " + price_id);
		try {
			Price instance = (Price) getCurrentSession().get(
					"com.formula.hibernate.Price", price_id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Price> findByExample(Price instance) {
		log.debug("finding Prices instance by example");
		try {
			List<Price> results = (List<Price>) getCurrentSession()
					.createCriteria("com.formula.hibernate.Price")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List<Price> findByProperty(String propertyName, Object value) {
		log.debug("finding Prices instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from price as model where model."
					+ propertyName + "= :val";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter("val", value);
			return (List<Price>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List<Price> findByProductDate(Object product, Object price_date) {
		log.debug("finding Prices instance with product: " + product
				+ ", price_date: " + price_date);
		try {
			String queryString = "from price as model where model.product"
					+ "= :product and model.price_date= :price_date order by model.price_date desc";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter("product", product);
			queryObject.setParameter("price_date", price_date);
			return (List<Price>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List<Price> findAll() {
		log.debug("finding all Brands instances");
		try {
			String queryString = "from price";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return (List<Price>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Price merge(Price detachedInstance) {
		log.debug("merging Prices instance");
		try {
			Price result = (Price) getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Price instance) {
		log.debug("attaching dirty Prices instance");
		try {
			getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Price instance) {
		log.debug("attaching clean Prices instance");
		try {
			getCurrentSession().buildLockRequest(LockOptions.NONE).lock(
					instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static PriceDAO getFromApplicationContext(ApplicationContext ctx) {
		return (PriceDAO) ctx.getBean("PriceDAO");
	}
}