package com.formula.hibernate.dao;

import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import static org.hibernate.criterion.Example.create;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.formula.hibernate.Line;

@Repository("line_dao")
@Scope("prototype")
@SuppressWarnings({"unchecked"})
public class LineDAO implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(LineDAO.class);
	// property constants
	public static final String LINE_NAME = "lineName";

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected void initDao() {
		// do nothing
	}

	public void save(Line transientInstance) {
		log.debug("saving Lines instance");
		try {
			getCurrentSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Line persistentInstance) {
		log.debug("deleting Lines instance");
		try {
			getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Line findById(java.lang.Long line_id) {
		log.debug("getting Lines instance with line_id: " + line_id);
		try {
			Line instance = (Line) getCurrentSession().get(
					"com.formula.hibernate.Line", line_id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Line> findByExample(Line instance) {
		log.debug("finding Lines instance by example");
		try {
			List<Line> results = (List<Line>) getCurrentSession()
					.createCriteria("com.formula.hibernate.Line")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List<Line> findByProperty(String propertyName, Object value) {
		log.debug("finding Lines instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from line as model where model."
					+ propertyName + "= :val";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter("val", value);
			return (List<Line>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List<Line> findByLineName(Object lineName) {
		return findByProperty(LINE_NAME, lineName);
	}

	public List<Line> findAll() {
		log.debug("finding all Brands instances");
		try {
			String queryString = "from line";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return (List<Line>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Line merge(Line detachedInstance) {
		log.debug("merging Lines instance");
		try {
			Line result = (Line) getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Line instance) {
		log.debug("attaching dirty Lines instance");
		try {
			getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Line instance) {
		log.debug("attaching clean Lines instance");
		try {
			getCurrentSession().buildLockRequest(LockOptions.NONE).lock(
					instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static LineDAO getFromApplicationContext(ApplicationContext ctx) {
		return (LineDAO) ctx.getBean("LineDAO");
	}
}