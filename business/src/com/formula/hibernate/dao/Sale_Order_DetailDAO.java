package com.formula.hibernate.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import static org.hibernate.criterion.Example.create;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.formula.hibernate.Sale_Order;
import com.formula.hibernate.Sale_Order_Detail;

@Repository("sale_order_detail_dao")
@Scope("prototype")
@SuppressWarnings({"unchecked"})
public class Sale_Order_DetailDAO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(Sale_Order_DetailDAO.class);

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected void initDao() {
		// do nothing
	}

	public Long getMax() {
		log.debug("getting the Max function");
		try {
			String queryString = "select max(od.detail_id) as detail_id from sale_order_detail od";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return (Long) queryObject.list().get(0);
		} catch (RuntimeException re) {
			log.error("get the max value failed", re);
			throw re;
		}
	}
	
	public List<Sale_Order_Detail> reading_List(Long order_id){
		Criteria query = getCurrentSession().createCriteria(Sale_Order.class);
		query.add(Restrictions.idEq(order_id));
		Sale_Order sale_order = (Sale_Order) query.uniqueResult();
		List<Sale_Order_Detail> lstSaleOrderDetail = new ArrayList<Sale_Order_Detail>(0);
		if (sale_order != null) {
			Set<Sale_Order_Detail> set_sale_orders = sale_order.getDetails();
			if (!set_sale_orders.isEmpty()) {
				lstSaleOrderDetail.addAll(set_sale_orders);
			}
		}
		return lstSaleOrderDetail;
	}

	public void save(Sale_Order_Detail transientInstance) {
		log.debug("saving Order_Details instance");
		try {
			getCurrentSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Sale_Order_Detail persistentInstance) {
		log.debug("deleting Order_Details instance");
		try {
			getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Sale_Order_Detail findById(java.lang.Long detail_id) {
		log.debug("getting Order_Details instance with detail_id: " + detail_id);
		try {
			Sale_Order_Detail instance = (Sale_Order_Detail) getCurrentSession().get(
					"com.formula.hibernate.Sale_Order_Detail", detail_id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Sale_Order_Detail> findByExample(Sale_Order_Detail instance) {
		log.debug("finding Sale_Order_Detail instance by example");
		try {
			List<Sale_Order_Detail> results = (List<Sale_Order_Detail>) getCurrentSession()
					.createCriteria("com.formula.hibernate.Sale_Order_Detail")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List<Sale_Order_Detail> findByProperty(String propertyName, Object value) {
		log.debug("finding Sale_Order_Detail instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from sale_order_detail as model where model."
					+ propertyName + "= :val";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter("val", value);
			return (List<Sale_Order_Detail>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List<Sale_Order_Detail> findAll() {
		log.debug("finding all Order_Details instances");
		try {
			String queryString = "from sale_order_detail";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return (List<Sale_Order_Detail>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Sale_Order_Detail merge(Sale_Order_Detail detachedInstance) {
		log.debug("merging Order_Details instance");
		try {
			Sale_Order_Detail result = (Sale_Order_Detail) getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Sale_Order_Detail instance) {
		log.debug("attaching dirty Order_Detail instance");
		try {
			getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Sale_Order_Detail instance) {
		log.debug("attaching clean Order_Detail instance");
		try {
			getCurrentSession().buildLockRequest(LockOptions.NONE).lock(
					instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static Sale_Order_DetailDAO getFromApplicationContext(ApplicationContext ctx) {
		return (Sale_Order_DetailDAO) ctx.getBean("Sale_Order_DetailDAO");
	}
}