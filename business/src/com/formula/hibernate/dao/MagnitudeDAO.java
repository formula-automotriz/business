package com.formula.hibernate.dao;

import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import static org.hibernate.criterion.Example.create;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.formula.hibernate.Magnitude;

@Repository("magnitude_dao")
@Scope("prototype")
@SuppressWarnings({"unchecked"})
public class MagnitudeDAO implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(MagnitudeDAO.class);
	// property constants
	public static final String MAGNITUDE_NAME = "magnitudeName";

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected void initDao() {
		// do nothing
	}

	public void save(Magnitude transientInstance) {
		log.debug("saving Magnitudes instance");
		try {
			getCurrentSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Magnitude persistentInstance) {
		log.debug("deleting Magnitudes instance");
		try {
			getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Magnitude findById(java.lang.Long magnitude_id) {
		log.debug("getting Magnitudes instance with magnitude_id: " + magnitude_id);
		try {
			Magnitude instance = (Magnitude) getCurrentSession().get(
					"com.formula.hibernate.Magnitude", magnitude_id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Magnitude> findByExample(Magnitude instance) {
		log.debug("finding Magnitudes instance by example");
		try {
			List<Magnitude> results = (List<Magnitude>) getCurrentSession()
					.createCriteria("com.formula.hibernate.Magnitude")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List<Magnitude> findByProperty(String propertyName, Object value) {
		log.debug("finding Magnitudes instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from magnitude as model where model."
					+ propertyName + "= :val";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter("val", value);
			return (List<Magnitude>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List<Magnitude> findByMagnitudeName(Object magnitudeName) {
		return findByProperty(MAGNITUDE_NAME, magnitudeName);
	}

	public List<Magnitude> findAll() {
		log.debug("finding all Magnitudes instances");
		try {
			String queryString = "from magnitude";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return (List<Magnitude>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Magnitude merge(Magnitude detachedInstance) {
		log.debug("merging Magnitudes instance");
		try {
			Magnitude result = (Magnitude) getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Magnitude instance) {
		log.debug("attaching dirty Magnitudes instance");
		try {
			getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Magnitude instance) {
		log.debug("attaching clean Magnitudes instance");
		try {
			getCurrentSession().buildLockRequest(LockOptions.NONE).lock(
					instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static MagnitudeDAO getFromApplicationContext(ApplicationContext ctx) {
		return (MagnitudeDAO) ctx.getBean("MagnitudeDAO");
	}
}