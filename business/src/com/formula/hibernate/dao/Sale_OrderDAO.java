package com.formula.hibernate.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import static org.hibernate.criterion.Example.create;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.formula.hibernate.Sale_Order;

@Repository("sale_order_dao")
@Scope("prototype")
@SuppressWarnings({"unchecked"})
public class Sale_OrderDAO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(Sale_OrderDAO.class);

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected void initDao() {
		// do nothing
	}

	public Long getMax() {
		log.debug("getting the Max function");
		try {
			String queryString = "select max(o.order_id) as order_id from sale_order o";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return (Long) queryObject.list().get(0);
		} catch (RuntimeException re) {
			log.error("get the max value failed", re);
			throw re;
		}
	}

	public void save(Sale_Order transientInstance) {
		log.debug("saving Orders instance");
		try {
			getCurrentSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Sale_Order persistentInstance) {
		log.debug("deleting Orders instance");
		try {
			getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Sale_Order findById(java.lang.Long order_id) {
		log.debug("getting Orders instance with order_id: " + order_id);
		try {
			Sale_Order instance = (Sale_Order) getCurrentSession().get(
					"com.formula.hibernate.Sale_Order", order_id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Sale_Order> findByExample(Sale_Order instance) {
		log.debug("finding Orders instance by example");
		try {
			List<Sale_Order> results = (List<Sale_Order>) getCurrentSession()
					.createCriteria("com.formula.hibernate.Sale_Order")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List<Sale_Order> findByProperty(String propertyName, Object value) {
		log.debug("finding Orders instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from sale_order as model where model."
					+ propertyName + "= :val";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter("val", value);
			return (List<Sale_Order>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List<Sale_Order> findAll() {
		log.debug("finding all Orders instances");
		try {
			String queryString = "from sale_order so join fetch so.client";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return (List<Sale_Order>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Sale_Order merge(Sale_Order detachedInstance) {
		log.debug("merging Orders instance");
		try {
			Sale_Order result = (Sale_Order) getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Sale_Order instance) {
		log.debug("attaching dirty Orders instance");
		try {
			getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Sale_Order instance) {
		log.debug("attaching clean Orders instance");
		try {
			getCurrentSession().buildLockRequest(LockOptions.NONE).lock(
					instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static Sale_OrderDAO getFromApplicationContext(ApplicationContext ctx) {
		return (Sale_OrderDAO) ctx.getBean("Sale_OrderDAO");
	}
}