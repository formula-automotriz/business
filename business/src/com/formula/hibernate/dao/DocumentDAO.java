package com.formula.hibernate.dao;

import static org.hibernate.criterion.Example.create;

import java.io.Serializable;
import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.formula.hibernate.Document;

@Repository("document_dao")
@Scope("prototype")
@SuppressWarnings({"unchecked"})
public class DocumentDAO implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(BrandDAO.class);

	// property constants
	public static final String DOCUMENT_NAME = "documentName";

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected void initDao() {
		// do nothing
	}

	public void save(Document transientInstance) {
		log.debug("saving Documents instance");
		try {
			getCurrentSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Document persistentInstance) {
		log.debug("deleting Documents instance");
		try {
			getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Document findById(java.lang.Long document_id) {
		log.debug("getting Documents instance with document_id: " + document_id);
		try {
			Document instance = (Document) getCurrentSession().get(
					"com.formula.hibernate.Document", document_id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Document> findByExample(Document instance) {
		log.debug("finding Documents instance by example");
		try {
			List<Document> results = (List<Document>) getCurrentSession()
					.createCriteria("com.formula.hibernate.Document")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List<Document> findByProperty(String propertyName, Object value) {
		log.debug("finding Documents instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from document as model where model."
					+ propertyName + "= :val";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter("val", value);
			return (List<Document>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public Document findByCode(String document_code) {
		log.debug("finding Document instance with document_code: " + document_code);
		try {
			String queryString = "from document as model where model.document_code = :val";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter("val", document_code);
			return (Document)queryObject.uniqueResult();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List<Document> findByDocumentName(Object documentName) {
		return findByProperty(DOCUMENT_NAME, documentName);
	}

	public List<Document> findAll() {
		log.debug("finding all Documents instances");
		try {
			String queryString = "from document";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return (List<Document>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Document merge(Document detachedInstance) {
		log.debug("merging Documents instance");
		try {
			Document result = (Document) getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Document instance) {
		log.debug("attaching dirty Documents instance");
		try {
			getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Document instance) {
		log.debug("attaching clean Documents instance");
		try {
			getCurrentSession().buildLockRequest(LockOptions.NONE).lock(
					instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static DocumentDAO getFromApplicationContext(ApplicationContext ctx) {
		return (DocumentDAO) ctx.getBean("DocumentDAO");
	}
}
