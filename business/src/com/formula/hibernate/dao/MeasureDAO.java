package com.formula.hibernate.dao;

import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import static org.hibernate.criterion.Example.create;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.formula.hibernate.Measure;

@Repository("measure_dao")
@Scope("prototype")
@SuppressWarnings({"unchecked"})
public class MeasureDAO implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(MeasureDAO.class);
	// property constants
	public static final String MEASURE_NAME = "measureName";

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected void initDao() {
		// do nothing
	}

	public void save(Measure transientInstance) {
		log.debug("saving Measures instance");
		try {
			getCurrentSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Measure persistentInstance) {
		log.debug("deleting Measures instance");
		try {
			getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Measure findById(java.lang.Long measure_id) {
		log.debug("getting Measures instance with measure_id: " + measure_id);
		try {
			Measure instance = (Measure) getCurrentSession().get(
					"com.formula.hibernate.Measure", measure_id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Measure> findByExample(Measure instance) {
		log.debug("finding Measures instance by example");
		try {
			List<Measure> results = (List<Measure>) getCurrentSession()
					.createCriteria("com.formula.hibernate.Measure")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List<Measure> findByProperty(String propertyName, Object value) {
		log.debug("finding Measures instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from measure as model where model."
					+ propertyName + "= :val";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter("val", value);
			return (List<Measure>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List<Measure> findByMeasureName(Object measureName) {
		return findByProperty(MEASURE_NAME, measureName);
	}

	public List<Measure> findAll() {
		log.debug("finding all Measures instances");
		try {
			String queryString = "from measure";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return (List<Measure>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Measure merge(Measure detachedInstance) {
		log.debug("merging Measures instance");
		try {
			Measure result = (Measure) getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Measure instance) {
		log.debug("attaching dirty Measures instance");
		try {
			getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Measure instance) {
		log.debug("attaching clean Measures instance");
		try {
			getCurrentSession().buildLockRequest(LockOptions.NONE).lock(
					instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static MeasureDAO getFromApplicationContext(ApplicationContext ctx) {
		return (MeasureDAO) ctx.getBean("MeasureDAO");
	}
}