package com.formula.hibernate.dao;

import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import static org.hibernate.criterion.Example.create;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.formula.hibernate.Client;

@Repository("client_dao")
@Scope("prototype")
@SuppressWarnings({"unchecked"})
public class ClientDAO implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ClientDAO.class);
	// property constants
	public static final String CLIENT_NAME = "clientName";

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected void initDao() {
		// do nothing
	}

	public void save(Client transientInstance) {
		log.debug("saving Clients instance");
		try {
			getCurrentSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Client persistentInstance) {
		log.debug("deleting Clients instance");
		try {
			getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Client findById(java.lang.Long client_id) {
		log.debug("getting Clients instance with client_id: " + client_id);
		try {
			Client instance = (Client) getCurrentSession().get(
					"com.formula.hibernate.Client", client_id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Client> findByExample(Client instance) {
		log.debug("finding Clients instance by example");
		try {
			List<Client> results = (List<Client>) getCurrentSession()
					.createCriteria("com.formula.hibernate.Client")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List<Client> findByProperty(String propertyName, Object value) {
		log.debug("finding Clients instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from client as model where model."
					+ propertyName + "= :val";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter("val", value);
			return (List<Client>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List<Client> findByClientName(Object clientName) {
		return findByProperty(CLIENT_NAME, clientName);
	}

	public List<Client> findAll() {
		log.debug("finding all Clients instances");
		try {
			String queryString = "from client";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return (List<Client>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Client merge(Client detachedInstance) {
		log.debug("merging Clients instance");
		try {
			Client result = (Client) getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Client instance) {
		log.debug("attaching dirty Clients instance");
		try {
			getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Client instance) {
		log.debug("attaching clean Clients instance");
		try {
			getCurrentSession().buildLockRequest(LockOptions.NONE).lock(
					instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static ClientDAO getFromApplicationContext(ApplicationContext ctx) {
		return (ClientDAO) ctx.getBean("ClientDAO");
	}
}