package com.formula.hibernate.dao;

import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import static org.hibernate.criterion.Example.create;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.formula.hibernate.Family;

@Repository("family_dao")
@Scope("prototype")
@SuppressWarnings({"unchecked"})
public class FamilyDAO implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(FamilyDAO.class);
	// property constants
	public static final String FAMILY_NAME = "familyName";

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected void initDao() {
		// do nothing
	}

	public void save(Family transientInstance) {
		log.debug("saving Families instance");
		try {
			getCurrentSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Family persistentInstance) {
		log.debug("deleting Families instance");
		try {
			getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Family findById(java.lang.Long family_id) {
		log.debug("getting Families instance with family_id: " + family_id);
		try {
			Family instance = (Family) getCurrentSession().get(
					"com.formula.hibernate.Family", family_id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Family> findByExample(Family instance) {
		log.debug("finding Families instance by example");
		try {
			List<Family> results = (List<Family>) getCurrentSession()
					.createCriteria("com.formula.hibernate.Family")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List<Family> findByProperty(String propertyName, Object value) {
		log.debug("finding Families instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from family as model where model."
					+ propertyName + "= :val";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter("val", value);
			return (List<Family>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List<Family> findByFamilyName(Object familyName) {
		return findByProperty(FAMILY_NAME, familyName);
	}

	public List<Family> findAll() {
		log.debug("finding all Families instances");
		try {
			String queryString = "from family";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return (List<Family>)queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Family merge(Family detachedInstance) {
		log.debug("merging Families instance");
		try {
			Family result = (Family) getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Family instance) {
		log.debug("attaching dirty Family instance");
		try {
			getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Family instance) {
		log.debug("attaching clean Families instance");
		try {
			getCurrentSession().buildLockRequest(LockOptions.NONE).lock(
					instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static FamilyDAO getFromApplicationContext(ApplicationContext ctx) {
		return (FamilyDAO) ctx.getBean("FamilyDAO");
	}
}