package com.formula.hibernate;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "client")
@Table(name = "CLIENT")
public class Client implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long client_id;
	private String client_name;
	private String client_code;
	private String category;
	private String address;
	private String provider;
	private String phone;
	private Set<Sale_Order> orders;
	
	public Client() {
		
	}

	public Client(Long client_id, String client_name, String category, String address, String provider, String phone, Set<Sale_Order> orders) {
		this.client_id = client_id;
		this.client_name = client_name;
		this.category = category;
		this.address = address;
		this.provider = provider;
		this.phone = phone;
		this.orders = orders;
	}

	@Id
	@Column(name = "client_id", unique = true, nullable = false, precision = 11, scale = 0)
	public Long getClient_id() {
		return client_id;
	}

	public void setClient_id(Long client_id) {
		this.client_id = client_id;
	}

	@Column(name = "client_name", length = 58)
	public String getClient_name() {
		return client_name;
	}

	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}

	@Column(name = "client_code", length = 50)
	public String getClient_code() {
		return client_code;
	}

	public void setClient_code(String client_code) {
		this.client_code = client_code;
	}

	@Column(name = "category", length = 1)
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Column(name = "address", length = 106)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "provider", length = 50)
	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	@Column(name = "phone", length = 105)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, mappedBy = "client")
	public Set<Sale_Order> getOrders() {
		return orders;
	}

	public void setOrders(Set<Sale_Order> orders) {
		this.orders = orders;
	}

	@Override
	public String toString() {
		//return "Client [client_id=" + client_id + ", client_name=" + client_name + "]";
		return String.format("%s[client_id=%d]", getClass().getSimpleName(),getClient_id());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((client_id == null) ? 0 : client_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (client_id == null) {
			if (other.client_id != null)
				return false;
		} else if (!client_id.equals(other.client_id))
			return false;
		return true;
	}
}