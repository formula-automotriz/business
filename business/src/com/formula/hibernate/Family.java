package com.formula.hibernate;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "family")
@Table(name = "FAMILY")
public class Family implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long family_id;
	private String family_name;
	private Set<Line> lines;
	
	public Family() {
		
	}

	public Family(Long family_id, String family_name, Set<Line> lines) {
		this.family_id = family_id;
		this.family_name = family_name;
		this.lines = lines;
	}

	@Id
	@Column(name = "family_id", unique = true, nullable = false, precision = 11, scale = 0)
	public Long getFamily_id() {
		return family_id;
	}

	public void setFamily_id(Long family_id) {
		this.family_id = family_id;
	}

	@Column(name = "family_name", length = 50)
	public String getFamily_name() {
		return family_name;
	}

	public void setFamily_name(String family_name) {
		this.family_name = family_name;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, mappedBy = "family")
	public Set<Line> getLines() {
		return lines;
	}

	public void setLines(Set<Line> lines) {
		this.lines = lines;
	}

	@Override
	public String toString() {
		//return "Family [family_id=" + family_id + ", family_name=" + family_name + "]";
		return String.format("%s[family_id=%d]", getClass().getSimpleName(),getFamily_id());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((family_id == null) ? 0 : family_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Family other = (Family) obj;
		if (family_id == null) {
			if (other.family_id != null)
				return false;
		} else if (!family_id.equals(other.family_id))
			return false;
		return true;
	}
}
