package com.formula.hibernate;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "product")
@Table(name = "PRODUCT")
public class Product implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long product_id;
	private String product_code;
	private String product_name;
	private Category category;
	private Line line;
	private Double content;
	private Measure measure;
	private Price price;
	private Integer type;
	private String bar_code;
	
	private Set<Sale_Order_Detail> details;
	
	public Product() {
		
	}

	public Product(Long product_id, String product_code, String product_name, Category category,
			Line line, Double content, Measure measure, Price price, Integer type, String bar_code, Set<Sale_Order_Detail> details) {
		this.product_id = product_id;
		this.product_code = product_code;
		this.product_name = product_name;
		this.category = category;
		this.line = line;
		this.content = content;
		this.measure = measure;
		this.price = price;
		this.type = type;
		this.bar_code = bar_code;
		this.details = details;
	}

	@Id
	@Column(name = "product_id", unique = true, nullable = false, precision = 11, scale = 0)
	public Long getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Long product_id) {
		this.product_id = product_id;
	}

	@Column(name = "product_code", length = 50)
	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}

	@Column(name = "product_name", length = 80)
	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name = "category_id")
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name = "line_id")
	public Line getLine() {
		return line;
	}

	public void setLine(Line line) {
		this.line = line;
	}

	@Column(name = "content", unique = true, nullable = false, precision = 11, scale = 6)
	public Double getContent() {
		return content;
	}

	public void setContent(Double content) {
		this.content = content;
	}

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name = "measure_id")
	public Measure getMeasure() {
		return measure;
	}

	public void setMeasure(Measure measure) {
		this.measure = measure;
	}

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name = "price_id")
	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	@Column(name = "type", unique = false, nullable = true, precision = 11, scale = 0)
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "bar_code", length = 20)
	public String getBar_code() {
		return bar_code;
	}

	public void setBar_code(String bar_code) {
		this.bar_code = bar_code;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "product")
	public Set<Sale_Order_Detail> getDetails() {
		return details;
	}

	public void setDetails(Set<Sale_Order_Detail> details) {
		this.details = details;
	}

	@Override
	public String toString() {
		return "Product [product_id=" + product_id + ", product_code=" + product_code + ", product_name=" + product_name
				+ ", category=" + category + ", line=" + line + ", content=" + content + ", measure=" + measure
				+ ", price=" + price + ", bar_code=" + bar_code + "]";
	}
}
