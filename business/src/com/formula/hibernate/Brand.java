package com.formula.hibernate;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "brand")
@Table(name = "BRAND")
public class Brand implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long brand_id;
	private String brand_name;
	private Set<Line> lines;
	
	public Brand() {
		
	}

	public Brand(Long brand_id, String brand_name, Set<Line> lines) {
		this.brand_id = brand_id;
		this.brand_name = brand_name;
		this.lines = lines;
	}

	@Id
	@Column(name = "brand_id", unique = true, nullable = false, precision = 11, scale = 0)
	public Long getBrand_id() {
		return brand_id;
	}

	public void setBrand_id(Long brand_id) {
		this.brand_id = brand_id;
	}

	@Column(name = "brand_name", length = 50)
	public String getBrand_name() {
		return brand_name;
	}

	public void setBrand_name(String brand_name) {
		this.brand_name = brand_name;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, mappedBy = "brand")
	public Set<Line> getLines() {
		return lines;
	}

	public void setLines(Set<Line> lines) {
		this.lines = lines;
	}

	@Override
	public String toString() {
		//return "Brand [brand_id=" + brand_id + ", brand_name=" + brand_name + "]";
		return String.format("%s[brand_id=%d]", getClass().getSimpleName(),getBrand_id());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brand_id == null) ? 0 : brand_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Brand other = (Brand) obj;
		if (brand_id == null) {
			if (other.brand_id != null)
				return false;
		} else if (!brand_id.equals(other.brand_id))
			return false;
		return true;
	}
}
