package com.formula.hibernate;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "measure")
@Table(name = "MEASURE")
public class Measure implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long measure_id;
	private String measure_code;
	private String measure_ext;
	private Double content;
	private Metric metric;
	private Set<Product> products;
	private Set<Sale_Order_Detail> details;
	
	public Measure() {
		
	}

	public Measure(Long measure_id, String measure_code, String measure_ext, Double content, Metric metric, Set<Product> products) {
		this.measure_id = measure_id;
		this.measure_code = measure_code;
		this.measure_ext = measure_ext;
		this.content = content;
		this.metric = metric;
		this.products = products;
	}

	@Id
	@Column(name = "measure_id", unique = true, nullable = false, precision = 11, scale = 0)
	public Long getMeasure_id() {
		return measure_id;
	}

	public void setMeasure_id(Long measure_id) {
		this.measure_id = measure_id;
	}

	@Column(name = "measure_code", length = 50)
	public String getMeasure_code() {
		return measure_code;
	}

	public void setMeasure_code(String measure_code) {
		this.measure_code = measure_code;
	}

	@Column(name = "measure_ext", length = 50)
	public String getMeasure_ext() {
		return measure_ext;
	}

	public void setMeasure_ext(String measure_ext) {
		this.measure_ext = measure_ext;
	}

	@Column(name = "content", unique = true, nullable = false, precision = 11, scale = 6)
	public Double getContent() {
		return content;
	}

	public void setContent(Double content) {
		this.content = content;
	}

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name = "metric_id")
	public Metric getMetric() {
		return metric;
	}

	public void setMetric(Metric metric) {
		this.metric = metric;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, mappedBy = "measure")
	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, mappedBy = "measure")
	public Set<Sale_Order_Detail> getDetails() {
		return details;
	}

	public void setDetails(Set<Sale_Order_Detail> details) {
		this.details = details;
	}

	@Override
	public String toString() {
		//return "Measure [measure_id=" + measure_id + ", measure_name=" + measure_name + ", measure_code=" + measure_code + "]";
		return String.format("%s[measure_id=%d]", getClass().getSimpleName(),getMeasure_id());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((measure_id == null) ? 0 : measure_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Measure other = (Measure) obj;
		if (measure_id == null) {
			if (other.measure_id != null)
				return false;
		} else if (!measure_id.equals(other.measure_id))
			return false;
		return true;
	}
}
