package com.formula.hibernate;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "sale_order_detail")
@Table(name = "SALE_ORDER_DETAIL")
public class Sale_Order_Detail implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long detail_id;
	private Sale_Order sale_order;
	private Product product;
	private Double quantity;
	private Measure measure;
	private Double selling;
	private Double discount;
	
	public Sale_Order_Detail() {
		
	}

	public Sale_Order_Detail(Long detail_id, Sale_Order sale_order, Product product, Double quantity, Measure measure, Double selling,
			Double discount) {
		this.detail_id = detail_id;
		this.sale_order = sale_order;
		this.product = product;
		this.quantity = quantity;
		this.measure = measure;
		this.selling = selling;
		this.discount = discount;
	}

	@Id
	@Column(name = "detail_id", unique = true, nullable = false, precision = 10, scale = 0)
	public Long getDetail_id() {
		return detail_id;
	}

	public void setDetail_id(Long detail_id) {
		this.detail_id = detail_id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_id")
	public Sale_Order getSale_order() {
		return sale_order;
	}

	public void setSale_order(Sale_Order sale_order) {
		this.sale_order = sale_order;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id")
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Column(name = "quantity", unique = false, nullable = true, precision = 11, scale = 2)
	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "measure_id")
	public Measure getMeasure() {
		return measure;
	}

	public void setMeasure(Measure measure) {
		this.measure = measure;
	}

	@Column(name = "selling", unique = false, nullable = true, precision = 11, scale = 2)
	public Double getSelling() {
		return selling;
	}

	public void setSelling(Double selling) {
		this.selling = selling;
	}

	@Column(name = "discount", unique = false, nullable = true, precision = 4, scale = 2)
	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	@Override
	public String toString() {
		//return "Order_Detail [detail_id=" + detail_id + ", order=" + order + ", product=" + product + ", quantity="
		//		+ quantity + ", selling=" + selling + ", discount=" + discount + "]";
		return String.format("%s[detail_id=%d]", getClass().getSimpleName(),getDetail_id());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((detail_id == null) ? 0 : detail_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sale_Order_Detail other = (Sale_Order_Detail) obj;
		if (detail_id == null) {
			if (other.detail_id != null)
				return false;
		} else if (!detail_id.equals(other.detail_id))
			return false;
		return true;
	}
}