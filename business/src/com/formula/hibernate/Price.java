package com.formula.hibernate;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "price")
@Table(name = "PRICE")
public class Price implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long price_id;
	private Document document;
	private Product product;
	private Double cost;
	private Double profit;
	private Double selling;
	private Date price_date;
	private User user;
	private Set<Product> products;
	
	public Price() {
		
	}

	public Price(Long price_id, Document document, Product product, Double cost, Double profit, Double selling, Date price_date, User user, Set<Product> products) {
		this.price_id = price_id;
		this.document = document;
		this.product = product;
		this.cost = cost;
		this.profit = profit;
		this.selling = selling;
		this.price_date = price_date;
		this.user = user;
		this.products = products;
	}

	@Id
	@Column(name = "price_id", unique = true, nullable = false, precision = 11, scale = 0)
	public Long getPrice_id() {
		return price_id;
	}

	public void setPrice_id(Long price_id) {
		this.price_id = price_id;
	}

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name = "document_id")
	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name = "product_id")
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Column(name = "cost", unique = false, nullable = false, precision = 11, scale = 2)
	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	@Column(name = "profit", unique = false, nullable = false, precision = 11, scale = 2)
	public Double getProfit() {
		return profit;
	}

	public void setProfit(Double profit) {
		this.profit = profit;
	}

	@Column(name = "selling", unique = false, nullable = false, precision = 11, scale = 2)
	public Double getSelling() {
		return selling;
	}

	public void setSelling(Double selling) {
		this.selling = selling;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "price_date", length = 7)
	public Date getPrice_date() {
		return price_date;
	}

	public void setPrice_date(Date price_date) {
		this.price_date = price_date;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, mappedBy = "price")
	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name = "user_id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		//return "Price [price_id=" + price_id + ", document=" + document + "]";
		return String.format("%s[price_id=%d]", getClass().getSimpleName(),getPrice_id());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((price_id == null) ? 0 : price_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Price other = (Price) obj;
		if (price_id == null) {
			if (other.price_id != null)
				return false;
		} else if (!price_id.equals(other.price_id))
			return false;
		return true;
	}
}
