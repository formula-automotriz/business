package com.formula.hibernate;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "document")
@Table(name = "DOCUMENT")
public class Document implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long document_id;
	private String document_code;
	private String document_name;
	private Set<Price> prices;
	
	public Document() {
		
	}

	public Document(Long document_id, String document_name) {
		this.document_id = document_id;
		this.document_name = document_name;
	}

	@Id
	@Column(name = "document_id", unique = true, nullable = false, precision = 11, scale = 0)
	public Long getDocument_id() {
		return document_id;
	}

	public void setDocument_id(Long document_id) {
		this.document_id = document_id;
	}

	@Column(name = "document_code", length = 50)
	public String getDocument_code() {
		return document_code;
	}

	public void setDocument_code(String document_code) {
		this.document_code = document_code;
	}

	@Column(name = "document_name", length = 50)
	public String getDocument_name() {
		return document_name;
	}

	public void setDocument_name(String document_name) {
		this.document_name = document_name;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, mappedBy = "document")
	public Set<Price> getPrices() {
		return prices;
	}

	public void setPrices(Set<Price> prices) {
		this.prices = prices;
	}

	@Override
	public String toString() {
		//return "Document [document_id=" + document_id + ", document_name=" + document_name + "]";
		return String.format("%s[document_id=%d]", getClass().getSimpleName(),getDocument_id());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((document_id == null) ? 0 : document_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Document other = (Document) obj;
		if (document_id == null) {
			if (other.document_id != null)
				return false;
		} else if (!document_id.equals(other.document_id))
			return false;
		return true;
	}
}
